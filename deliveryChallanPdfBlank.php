<?php
require('./fpdf.php');
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnId               = "";
  $grnDetailId         = "";
  $grnPrefix           = "";
  $grnNo               = "";
  $grnDate             = "";
  $poNo                = "";
  $poDate              = "";
  $custName            = "";
  $custCode            = "";
  $remarks             = "";
  $contPerson          = "";
  $phNo                = "";
  $userName            = "";
  $itemId              = "";
  $selectGrnEntryRes   = 0;
  $selectGrnMasterRes  = 0;
  $msg                 = "";
  $grnDetail           = array();
  // Pdf Create :Start
  $pdf = new FPDF('P','mm','A4');
  $pdf->AliasNbPages();
  $pdf->AddPage();
    
  //SELECT OF GRN MASTER :START
  $selectGrnMaster = "SELECT grnPrefix,grnNo,DATE_FORMAT(grnDate,'%d-%m-%Y') AS grnDate,poNo,DATE_FORMAT(poDate,'%d-%m-%Y') AS poDate,
                             custName,custCode,remarks,gatepass.userName,gatepass.gatePassId
                        FROM grnmaster
                        JOIN customer
                        JOIN gatepass
                       WHERE gatepass.gatePassId = ".$_GET['gatePassId']."
                         AND grnmaster.grnId = gatepass.grnId
                         AND grnmaster.customerId = customer.customerId";
  $selectGrnMasterRes = mysql_query($selectGrnMaster);
  if($selectGrnMasterRow = mysql_fetch_array($selectGrnMasterRes))
  {
   
  }
  //SELECT OF GRN MASTER :END
  //SELECT OF GRN DETAIL :START
  $a  = 0;
  $selectGrnEntry  = "SELECT grnDetail.grnDetailId,grndetail.grnId,item.itemName,grndetail.itemCode,grndetail.rangeValue,grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,DATE_FORMAT(custReqDate,'%d-%m-%Y') AS custReqDate,DATE_FORMAT(expDelivDate,'%d-%m-%Y') AS expDelivDate,
                             parameterentry.parameterName,gatepass.gatePassId,gatepassdetail.gatePassId,gatepassdetail.grnDetailId
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                        JOIN gatepassdetail
                        JOIN gatepass
                       WHERE gatepassdetail.gatePassId = ".$_GET['gatePassId']."
                         AND grnDetail.grnDetailId = gatepassdetail.grnDetailId
                         AND grndetail.parameterId = parameterentry.parameterId
                         AND grndetail.itemId      = item.itemId 
                         AND gatepass.gatePassId = gatepassdetail.gatePassId 
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    if(($a % $cfgRecPerGrnPage) == 0)
    {
      $yPosition = 65;
      $yPositionSecondPart = 215;
      if($a > 0)
        $pdf->AddPage();
      pageHeader();
      pageHeaderSecondPart();
      grnmasterFunc();
      grnmasterFuncSecondPart();
    }
    gatePassDetailFunc($yPosition,$a+1);
    $yPosition += 5;
    
    gatePassFuncSecondPart($yPositionSecondPart,$a+1);
    $yPositionSecondPart += 5;
    
    $a++;
  }
  $pdf->output();
  include("./bottom.php");
}
// First Part Of grn headerEnd And Footer part: Start

function grnmasterFunc()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(80,5);
    $pdf->Write(1,'Delivery Challan');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,12);
    $pdf->Write(1,'DOC. NO.: ST F 03');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,22);
    $pdf->Write(1,'Grn No.   :  ');
    $pdf->SetXY(12,17);
    $pdf->cell(43,20,'No.          :  ','1', '0', 'L');
    $pdf->SetXY(12,32);
    $pdf->Write(1,'Date        :  ');
    $pdf->SetXY(30,25);
    $pdf->Write(5,''.('__________'));
    $pdf->SetXY(30,30);
    $pdf->Write(5,''.('__________'));
        
    $pdf->SetXY(21,47);
    $pdf->Write(5,''.('_______________________________________________________________________________________'));
    $pdf->SetXY(12,53);
    $pdf->Write(5,''.('____________________________________________________________________________________________'));
    $pdf->SetXY(12,47);
    $pdf->Write(5,'M/S : ');
    // master Meter Footer : Start 
     $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'');
    $pdf->SetXY(10,115);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(13,125);
    $pdf->Write(5,'Received  By: ');
    $pdf->SetXY(50,113);
    $pdf->Write(5,'');
    $pdf->SetXY(50,115);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(53,125);
    $pdf->Write(5,'Receiver Name: ');
    $pdf->SetXY(155,113);
    $pdf->Write(5,'');
    $pdf->SetXY(150,115);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(153,125);
    $pdf->Write(5,'Prepared By: ');
    $pdf->SetXY(130,130);
    $pdf->Write(5,'For & On behalf of Krishna Instrumensts. ');
    $pdf->SetXY(10,135);
    $pdf->Write(5,'Note: Receive the Above Goods In Order And Good Condtions. ');
    // master Meter Footer : End
}

// Second Part Of grn headerEnd And Footer part: Start

function grnmasterFuncSecondPart()
{
    global $pdf;
    global $selectGrnMasterRow;
    $pdf->SetFont('Arial','',13);
    $pdf->SetXY(80,160);
    $pdf->Write(1,'Delivery Challan');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,160);
    $pdf->Write(1,'DOC. NO.: ST F 03');
    $pdf->SetFont('Arial','',10);
    $pdf->SetXY(12,169);
    $pdf->Write(1,'Grn No.   : ');
    $pdf->SetXY(12,165);
    $pdf->cell(43,20,'No.          :','1', '0', 'L');
    $pdf->SetXY(12,180);
    $pdf->Write(1,'Date        : ');
    $pdf->SetXY(29,172);
    $pdf->Write(5,''.('____________'));
    $pdf->SetXY(29,178);
    $pdf->Write(5,''.('____________'));
        
    $pdf->SetXY(21,198);
    $pdf->Write(5,''.('_______________________________________________________________________________________'));
    $pdf->SetXY(12,203);
    $pdf->Write(5,''.('____________________________________________________________________________________________'));
    $pdf->SetXY(12,198);
    $pdf->Write(5,'M/S : ');
    // master Meter Footer : Start 
     $pdf->SetFont('Arial','',10);
    $pdf->Write(5,'');
    $pdf->SetXY(10,260);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(13,265);
    $pdf->Write(5,'Received  By: ');
    $pdf->SetXY(50,260);
    $pdf->Write(5,'');
    $pdf->SetXY(50,260);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(53,265);
    $pdf->Write(5,'Receiver Name: ');
    $pdf->SetXY(155,257);
    $pdf->Write(5,'');
    $pdf->SetXY(150,260);
    $pdf->Write(5,''.('_______________'));
    $pdf->SetXY(153,265);
    $pdf->Write(5,'Prepared By: ');
    $pdf->SetXY(130,270);
    $pdf->Write(5,'For & On behalf of Krishna Instrumensts. ');
    $pdf->SetXY(10,271);
    $pdf->Write(5,'Note: Receive the Above Goods In Order And Good Condtions. ');
    // master Meter Footer : End
}
// First Part Of grn headerEnd And Footer part: End 
// Header Part Of grn Print pdf:Start 
function pageHeader()
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,2,70,30);

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,60);
  $pdf->cell(15,05,'SR.NO.','1', '0', 'L');

  $pdf->SetXY(28,60);
  $pdf->cell(165,05,'PARTICULARS','1', '0', 'L');
  
  $pdf->SetXY(173,60);
  $pdf->cell(20,05,'QUANTITY','1', '0', 'L');
}
//First Part Of Detail Of Grn :Start
//second part page Header :Start 
function pageHeaderSecondPart()
{
  global $pdf;
  $pdf->Image('./images/logo.jpg',122,160,70,30);

  $pdf->SetFont('Arial','',7);
  $pdf->SetXY(13,210);
  $pdf->cell(15,05,'SR.NO.','1', '0', 'L');

  $pdf->SetXY(28,210);
  $pdf->cell(165,05,'PARTICULARS','1', '0', 'L');
  
  $pdf->SetXY(173,210);
  $pdf->cell(20,05,'QUANTITY','1', '0', 'L');
}
//second part page Header :Start 
function gatePassDetailFunc($yPosition,$srNo)
{
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPosition);
  $pdf->cell(180,05,'','1', '0', 'L');
  $pdf->SetXY(28,$yPosition);
  $pdf->Cell(165,05,'','1', '0', 'L');
  $pdf->SetXY(173,$yPosition);
  $pdf->cell(20,05,'','1', '0', 'L');
}

function gatePassFuncSecondPart($yPositionSecondPart,$srNo)
{
  global $pdf;
  global $selectGrnEntryResRow;
  $pdf->SetXY(13,$yPositionSecondPart);
  $pdf->cell(180 ,05,'','1', '0', 'L');
  $pdf->SetXY(28,$yPositionSecondPart);
  $pdf->Cell(165,05,'','1', '0', 'L');
  $pdf->SetXY(173,$yPositionSecondPart);
  $pdf->cell(20,05,'','1', '0', 'L');
}
//First Part Of Detail Of Grn :End
?>