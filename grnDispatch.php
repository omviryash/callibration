<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $a                  = 0;
  $grnNo              = "";
  $grnPrefix          = "";
  $infoSheetNo        = "";
  $srNo               = "";
  $grnDate            = "";
  $itemCode           = "";
  $description        = "";
  $challan            = "";
  $received           = "";
  $condition          = "";
  $useCustId          = "";
  $poNo               = "";
  $poDate             = "";
  $contPerson         = "";
  $phNo               = "";
  $poDate             = "";
  $remarks            = "";
  $expDelivDate       = "";
  $customerId         = "";
  $custName           = "";
  $address            = "";
  $range              = "";
  $insertvar          = "";
  $gatePassMaxId      = "";
  $grnPrefixSelected  = "";
  $grnIdSelected      = "";
  $grnNoSelected      = "";
  $grnDateSelected    = "";
  $poNoSelected       = "";
  $poDateSelected     = "";
  $custIdSelected     = "";
  $custCodeSelected   = "";
  $contPersonSelected = "";
  $phNoSelected       = "";
  $remarksSelected    = "";
  $customerIdSelected = "";
  $parameterId        = array();
  $parameterName      = array();
  $parameterhidden    = array();
  $grnDetail          = array();
  //Cancel:Start
  if(isset($_POST['cancelBtn']))
  {
      header("Location:grnList.php");
      exit();
  }
  //Cancel:End
  if(isset($_POST['grnDateDay']))
  {
    $queryValue = $_POST['gatePassId'].", ";    
    $insertGateMatserQuery = "INSERT INTO gatepass (gatePassId,gatePassDate,grnId,userName,deliveryMode)
                              VALUES (".$queryValue."'".$_POST['grnDateYear']."-".$_POST['grnDateMonth']."-".$_POST['grnDateDay']."',".$_GET['grnId'].",'".$_SESSION['s_activId']."','".$_POST['deliveryMode']."')";
    $insertGateMaterQueryRes = mysql_query($insertGateMatserQuery);
    $gatePassIdInserted = mysql_insert_Id();
    foreach ($_POST['dispatch'] as $key => $value)
    {
         $updateDispatchQuery = "UPDATE grndetail 
                                     SET reportNo = '".$_POST['reportNo'][$key]."',
                                         gatePassId = ".$gatePassIdInserted.",
                                         dispatch = ".$value."
                               WHERE grndetailId = ".$key;
         $updateDispatchQueryRes = mysql_query($updateDispatchQuery);
    }
	
	if(isset($_REQUEST['grnPrintNewAddrBtn']) || isset($_REQUEST['infoPrintNewAddrBtn']))
		header("Location: deliveryChallanPdf_newaddr.php?display=info&gatePassId=".$gatePassIdInserted); 
	else 
		header("Location: deliveryChallanPdf.php?display=info&gatePassId=".$gatePassIdInserted); 
    exit();
  }
  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer";
  $selectNameResult = mysql_query($selectName);
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId [$a] = $nameRow['customerId'];
    $custName [$a]   = $nameRow['custName'];
    $custCode[$a]    = $nameRow['custCode'];
    $address[$a]     = $nameRow['address'];
    $a++;
  }
  ///////////////// GRN Prefix Combo : Starts
  $b = 0;
  $selectgrnPrefix = "SELECT grnPrefixId,grnPrefix
                        FROM grnprefix
                       ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($selectgrnPrefix);
  while($grnPrefixRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixId [$b]  = $grnPrefixRow['grnPrefixId'];
    $grnPrefix [$b]    = $grnPrefixRow['grnPrefix'];
    $b++;
  }
  ///////////////// GRN Prefix Combo : Ends
  ///////////////// Item Name Combo : Starts
  $itemId = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]  = $itemRow['itemId'];
    $itemName [$d]  = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends
  ///////////////// parameter Name Combo : Starts
  $parameterId = "";
  $parameterName = "";
  $p = 0;

  $selectparameter = "SELECT parameterId,parameterName
                        FROM parameterentry
                       ORDER BY parameterName";
  $selectparameterResult = mysql_query($selectparameter);


  while($parameterRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterId [$p]  = $parameterRow['parameterId'];
    $parameterName [$p]  = $parameterRow['parameterName'];
    $p++;
  }
  ///////////////// parameter Name Combo : Ends
  //////////////// Selection Of Gate Pass:Start
  $selectGatePassQuery = "SELECT (Max(gatePassId+1)) AS gatePassId 
                            FROM gatepass";
  $selectGatePassQueryRes = mysql_query($selectGatePassQuery);
  if($gatePassRow = mysql_fetch_assoc($selectGatePassQueryRes))
    $gatePassMaxId = (int)$gatePassRow['gatePassId'];
  //////////////// Selection Of Gate Pass:End
  ///////////////// Listing of GRN Master :  Starts
  $msg       = "";
  $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,infoSheetNo,grnmaster.grnDate,poNo,
                            poDate,custName,custCode,customer.address,grnmaster.contPerson,phNo,
                            remarks,grnmaster.customerId
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnId =".$_GET['grnId'];
  $grnmasterQueryResult = mysql_query($grnmasterQuery);

  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnPrefixSelected  = $grnListRow['grnPrefix'];
    $grnIdSelected      = $grnListRow['grnId'];
    $grnNoSelected      = $grnListRow['grnNo'];
    $infoSheetNo        = $grnListRow['infoSheetNo'];
    $grnDateSelected    = $grnListRow['grnDate'];
    $poNoSelected       = $grnListRow['poNo'];
    $poDateSelected     = $grnListRow['poDate'];
    $custIdSelected     = $grnListRow['customerId'];
    $custCodeSelected   = $grnListRow['custCode'];
    $addressSelected    = $grnListRow['address'];
    $contPersonSelected = $grnListRow['contPerson'];
    $phNoSelected       = $grnListRow['phNo'];
    $remarksSelected    = $grnListRow['remarks'];
    $customerIdSelected = $grnListRow['customerId'];
  }
  ///////////////// Listing of GRN Master : Ends
  ///////////////// Listing of GRN Detail : Starts
   $selectGrnEntry  = "SELECT grndetail.grnDetailId,grndetail.grnId,grndetail.itemId,item.itemName,grndetail.rangeValue,grndetail.itemCode,grndetail.description,
                             grndetail.reportNo,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,expDelivDate,grndetail.parameterId,grndetail.dispatch,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                         AND grndetail.dispatch = 0
                       ORDER BY grndetail.grnDetailId";

  $selectGrnEntryRes   = mysql_query($selectGrnEntry);
  $prevItemId = 0;
  $a  = 0;
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    $prevItemId = $selectGrnEntryResRow['grnDetailItemId'];
    $grnDetail[$a]['grnDetailId']  = $selectGrnEntryResRow['grnDetailId'];
    $grnDetail[$a]['grnId']        = $selectGrnEntryResRow['grnId'];
    $grnDetail[$a]['itemId']       = $selectGrnEntryResRow['itemId'];
    $grnDetail[$a]['itemName']     = $selectGrnEntryResRow['itemName'];
    $grnDetail[$a]['itemCode']     = $selectGrnEntryResRow['itemCode'];
    $grnDetail[$a]['range']        = $selectGrnEntryResRow['rangeValue'];
    $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
    $grnDetail[$a]['reportNo']     = $selectGrnEntryResRow['reportNo'];
    $grnDetail[$a]['challan']      = $selectGrnEntryResRow['challan'];
    $grnDetail[$a]['received']     = $selectGrnEntryResRow['received'];
    $grnDetail[$a]['grnCondition'] = $selectGrnEntryResRow['grnCondition'];
    $grnDetail[$a]['custReqDate']  = $selectGrnEntryResRow['custReqDate'];
    $grnDetail[$a]['expDelivDate'] = $selectGrnEntryResRow['expDelivDate'];
    $grnDetail[$a]['parameterId']  = $selectGrnEntryResRow['parameterId'];
    $grnDetail[$a]['parameterName']= $selectGrnEntryResRow['parameterName'];
    $grnDetail[$a]['description']  = $selectGrnEntryResRow['description'];
    $a++;
  }
//SELECT OF GRN DETAIL :END
  //grnCondition
  $grnCondition[] = "Ok";
  $grnCondition[] = "Faulty";
  $grnCondition[] = "NotChecked";
  //grnCondition

  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("grnIdSelected",$grnIdSelected);
  $smarty->assign("grnPrefixSelected",$grnPrefixSelected);
  $smarty->assign("grnNoSelected",$grnNoSelected);
  $smarty->assign("infoSheetNo",$infoSheetNo);
  $smarty->assign("grnDateSelected",$grnDateSelected);
  $smarty->assign("poNoSelected",$poNoSelected);
  $smarty->assign("poDateSelected",$poDateSelected);
  $smarty->assign("custIdSelected",$custIdSelected);
  $smarty->assign("custCodeSelected",$custCodeSelected);
  $smarty->assign("addressSelected",$addressSelected);
  $smarty->assign("contPersonSelected",$contPersonSelected);
  $smarty->assign("phNoSelected",$phNoSelected);
  $smarty->assign("remarksSelected",$remarksSelected);
  $smarty->assign("customerIdSelected",$customerIdSelected);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("address",$address);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("srNo",$srNo);
  $smarty->assign("itemCode",$itemCode);
  $smarty->assign("range",$range);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("description",$description);
  $smarty->assign("expDelivDate",$expDelivDate);
  $smarty->assign("challan",$challan);
  $smarty->assign("received",$received);
  $smarty->assign("condition",$condition);
  $smarty->assign("parameterhidden",$parameterhidden);
  $smarty->assign("grnDetail",$grnDetail);
  $smarty->assign("grnCondition",$grnCondition);
  $smarty->assign("gatePassMaxId",$gatePassMaxId);
  $smarty->display("grnDispatch.tpl");
}
?>