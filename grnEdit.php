<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnNo              = "";
  $grnPrefix          = "";
  $infoSheetNo        = "";
  $srNo               = "";
  $grnDate            = "";
  $itemCode           = "";
  $description        = "";
  $challan            = "";
  $received           = "";
  $condition          = "";
  $useCustId          = "";
  $poNo               = "";
  $poDate             = "";
  $contPerson         = "";
  $phNo               = "";
  $poDate             = "";
  $remarks            = "";
  $expDelivDate       = "";
  $customerId         = "";
  $custName           = "";
  $address            = "";
  $range              = "";
  $refGrnDetailId     = "";
  $a                  = 0;
  $insertvar          = "";
  $parameterId        = array();
  $parameterName      = array();
  $parameterhidden    = array();
  $grnDetail          = array();
  $grnPrefixSelected  = "";
  $grnIdSelected      = "";
  $grnNoSelected      = "";
  $grnDateSelected    = "";
  $poNoSelected       = "";
  $poDateSelected     = "";
  $custIdSelected     = "";
  $custCodeSelected   = "";
  $addressSelected    = "";
  $contPersonSelected = "";
  $phNoSelected       = "";
  $remarksSelected    = "";
  $customerIdSelected = "";
  if(isset($_POST['grnDateDay']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php");
      exit();
    }
    else if(isset($_POST['searchBtn']))
    {
      header("Location: grnList.php");
      exit();
    }
   
    if(isset($_POST['custName']) && strlen($_POST['custName']) > 0)
    { 
      $insertCustQuery = "INSERT INTO customer(custName,custCode,address)
                           VALUE ('".$_POST['custName']."','".$_POST['custCode']."','".$_POST['address']."')";
      $insertCustQueryRes = mysql_query($insertCustQuery);
      if(!$insertCustQueryRes)
        die("Insert Query Not Inserted : ".mysql_error());

      $useCustId = mysql_insert_id();
    }
    else
    
    $useCustId      = $_POST['customerId'];
    $grnDate        = $_POST['grnDateYear']."-".$_POST['grnDateMonth']."-".$_POST['grnDateDay'];
    $poDate         = $_POST['poDateYear']."-".$_POST['poDateMonth']."-".$_POST['poDateDay'];
//    -------------------------------;
    $updateGrnEntry = "UPDATE grnmaster
                          SET grnPrefix = '".$_POST['grnPrefix']."', grnNo = '".$_POST['grnNo']."', infoSheetNo = '".$_POST['infoSheetNo']."', grnDate = '".$grnDate."',
                              poNo = '".$_POST['poNo']."', customerId = '".$useCustId."', poDate = '".$poDate."',
                              contPerson = '".$_POST['contPerson']."', phNo = '".$_POST['phNo']."', remarks = '".$_POST['remarks']."',
                              userName = '".$_SESSION['s_activId']."'
                        WHERE grnId = ".$_POST['grnId'];
    $updateGrnEntryResult = mysql_query($updateGrnEntry);
    if(!$updateGrnEntryResult)
      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$updateGrnEntry);
    //--------------------------------
//    $deleteGrnDetail = "DELETE FROM grndetail
//                         WHERE grnId = ".$_POST['grnId'];
//    $deleteGrnDetailResult = mysql_query($deleteGrnDetail);
    
    $loopCount = 0;
    while($loopCount < count($_POST['itemId']))
    {
      $itemId         = ($_POST['itemId'][$loopCount] != '') ? $_POST['itemId'][$loopCount] : 0;
      $itemCode       = ($_POST['itemCode'][$loopCount] != '') ? $_POST['itemCode'][$loopCount] : 0;
      $parameterId    = ($_POST['parameterId'][$loopCount] != '') ? $_POST['parameterId'][$loopCount] : 0;
      $range          = ($_POST['range'][$loopCount] != '') ? $_POST['range'][$loopCount] : 0;
      $custReqDate    = ($_POST['custReqDate1'][$loopCount] != '') ? $_POST['custReqDate1'][$loopCount] : 0;
      $expDelivDate   = ($_POST['expDelivDate'][$loopCount] != '') ?  $_POST['expDelivDate'][$loopCount*3+2]['Year']."-".$_POST['expDelivDate'][$loopCount*3+1]['Month']."-".$_POST['expDelivDate'][$loopCount*3]['Day'] : 0;
      $description    = ($_POST['description'][$loopCount] != '') ? $_POST['description'][$loopCount] : 0;
      $challan        = ($_POST['challang'][$loopCount] != '') ? $_POST['challang'][$loopCount] : 0;
      $received       = ($_POST['receivedg'][$loopCount] != '') ? $_POST['receivedg'][$loopCount] : 0;
      $condition      = ($_POST['condition'][$loopCount] != '') ? $_POST['condition'][$loopCount] : 0;
      
      if($_POST['itemId'][$loopCount] != "" || $_POST['itemCode'][$loopCount] != "" || $_POST['parameterId'][$loopCount] != "" || $_POST['range'][$loopCount] != "" || $_POST['description'][$loopCount] != "" ||
         $_POST['chllang'][$loopCount] != "" || $_POST['receivedg'][$loopCount] != "" || $_POST['condition'][$loopCount] != '')
      {
        if(isset($_POST['grnDetailId'][$loopCount]) && $_POST['grnDetailId'][$loopCount] > 0)
        {
          $updateGrnDetail = "UPDATE grndetail
                                 SET itemId = ".$itemId.",
                                     itemCode = '".$itemCode."',
                                     parameterId = '".$parameterId."',
                                     rangeValue = '".$range."',
                                     custReqDate = '".$custReqDate."',
                                     expDelivDate = '".$expDelivDate."',
                                     description = '".$description."',
                                     challan = '".$challan."',
                                     received = '".$received."',
                                     grnCondition = '".$condition."'
                               WHERE grnDetailId = ".$_POST['grnDetailId'][$loopCount];
          $updateGrnDetailRes = mysql_query($updateGrnDetail);
        }
        else
        {
          $insertQueryGrnDetail = "INSERT INTO grndetail(grnId,itemId,itemCode,parameterId,rangeValue,custReqDate,expDelivDate,description,challan,received,grnCondition)
                                   VALUES (".$_POST['grnId'].",".$itemId.",'".$itemCode."','".$parameterId."','".$range."','".$custReqDate."','".$expDelivDate."','".$description."','".$challan."','".$received."','".$condition."')";
          $insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
          $grnDetailId = mysql_insert_id();
        }
      }
      $loopCount++;
    }
    if(isset($_POST['grnPrintBtn']))
      header("Location: grnPrintpdf.php?grnId=".$grnId);
    else if(isset($_POST['infoPrintBtn']))
      header("Location: grnPrintpdfInfo.php?grnId=".$grnId);
    else
      header("Location: grnEntry.php");
    exit();
  }

  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer
                  ORDER BY custName";
  $selectNameResult = mysql_query($selectName);
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId[$a] = $nameRow['customerId'];
    $custName[$a] = $nameRow['custName'];
    $custCode[$a] = $nameRow['custCode'];
    $address[$a] = $nameRow['address'];
    $a++;
  }
  ///////////////// GRN Prefix Combo : Starts
  $b = 0;
  $selectgrnPrefix = "SELECT grnPrefixId,grnPrefix
                        FROM grnprefix
                       ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($selectgrnPrefix);


  while($grnPrefixRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixId[$b] = $grnPrefixRow['grnPrefixId'];
    $grnPrefix[$b] = $grnPrefixRow['grnPrefix'];
    $b++;
  }
  ///////////////// GRN Prefix Combo : Ends
  ///////////////// Item Name Combo : Starts
  $itemId = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]  = $itemRow['itemId'];
    $itemName [$d]  = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends
  ///////////////// parameter Name Combo : Starts
  $parameterId = "";
  $parameterName = "";
  $p = 0;

  $selectparameter = "SELECT parameterId,parameterName
                        FROM parameterentry
                       ORDER BY parameterName";
  $selectparameterResult = mysql_query($selectparameter);


  while($parameterRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterId [$p]  = $parameterRow['parameterId'];
    $parameterName [$p]  = $parameterRow['parameterName'];
    $p++;
  }
  ///////////////// parameter Name Combo : Ends
  ///////////////// Listing of GRN Master :  Starts
  $msg = "";
  $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,infoSheetNo,grnmaster.grnDate,poNo,
                            poDate,custName,custCode,customer.address,grnmaster.contPerson,phNo,
                            remarks,grnmaster.customerId
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId = customer.customerId
                        AND grnId =".$_GET['grnId'];
  $grnmasterQueryResult = mysql_query($grnmasterQuery);

  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnPrefixSelected  = $grnListRow['grnPrefix'];
    $grnIdSelected      = $_GET['grnId'];
    $grnNoSelected      = $grnListRow['grnNo'];
    $infoSheetNo        = $grnListRow['infoSheetNo'];
    $grnDateSelected    = $grnListRow['grnDate'];
    $poNoSelected       = $grnListRow['poNo'];
    $poDateSelected     = $grnListRow['poDate'];
    $custIdSelected     = $grnListRow['customerId'];
    $custCodeSelected   = $grnListRow['custCode'];
    $addressSelected    = $grnListRow['address'];
    $contPersonSelected = $grnListRow['contPerson'];
    $phNoSelected       = $grnListRow['phNo'];
    $remarksSelected    = $grnListRow['remarks'];
    $customerIdSelected = $grnListRow['customerId'];
  }
  ///////////////// Listing of GRN Master : Ends
  ///////////////// Listing of GRN Detail : Starts
  $selectGrnEntry  = "SELECT grnDetailId,grndetail.refGrnDetailId,grndetail.grnId,grndetail.itemId,item.itemName,grndetail.rangeValue,grndetail.itemCode,
                             grndetail.description,grndetail.challan,grndetail.received,
                             grndetail.grnCondition,custReqDate,expDelivDate,grndetail.parameterId,
                             parameterentry.parameterName, grndetail.itemId AS grnDetailItemId
                        FROM grndetail
                        JOIN item
                        JOIN parameterentry
                       WHERE grnId = ".$_GET['grnId']."
                         AND grndetail.itemId = item.itemId
                         AND grndetail.parameterId = parameterentry.parameterId
                       ORDER BY grndetail.grnDetailId";
  $selectGrnEntryRes   = mysql_query($selectGrnEntry);

  $prevItemId = 0;
  $a  = 0;
  while($selectGrnEntryResRow = mysql_fetch_array($selectGrnEntryRes))
  {
    $prevItemId = $selectGrnEntryResRow['grnDetailItemId'];
    $grnDetail[$a]['grnDetailId']    = $selectGrnEntryResRow['grnDetailId'];
    $grnDetail[$a]['refGrnDetailId'] = $selectGrnEntryResRow['refGrnDetailId'];
    $grnDetail[$a]['grnId']          = $selectGrnEntryResRow['grnId'];
    $grnDetail[$a]['itemId']         = $selectGrnEntryResRow['itemId'];
    $grnDetail[$a]['itemName']       = $selectGrnEntryResRow['itemName'];
    $grnDetail[$a]['itemCode']       = $selectGrnEntryResRow['itemCode'];
    $grnDetail[$a]['range']          = $selectGrnEntryResRow['rangeValue'];
    $grnDetail[$a]['description']    = $selectGrnEntryResRow['description'];
    $grnDetail[$a]['challan']        = $selectGrnEntryResRow['challan'];
    $grnDetail[$a]['received']       = $selectGrnEntryResRow['received'];
    $grnDetail[$a]['grnCondition']   = $selectGrnEntryResRow['grnCondition'];
    $grnDetail[$a]['custReqDate']    = $selectGrnEntryResRow['custReqDate'];
    $grnDetail[$a]['expDelivDate']   = $selectGrnEntryResRow['expDelivDate'];
    $grnDetail[$a]['parameterId']    = $selectGrnEntryResRow['parameterId'];
    $grnDetail[$a]['parameterName']  = $selectGrnEntryResRow['parameterName'];
    $a++;
  }
//SELECT OF GRN DETAIL :END
  //grnCondition
  $grnCondition[] = "Ok";
  $grnCondition[] = "Faulty";
  $grnCondition[] = "NotChecked";
  //grnCondition

  include("./bottom.php");
  $smarty->assign("msg",$msg);
  $smarty->assign("grnIdSelected",$grnIdSelected);
  $smarty->assign("grnPrefixSelected",$grnPrefixSelected);
  $smarty->assign("grnNoSelected",$grnNoSelected);
  $smarty->assign("infoSheetNo",$infoSheetNo);
  $smarty->assign("grnDateSelected",$grnDateSelected);
  $smarty->assign("poNoSelected",$poNoSelected);
  $smarty->assign("poDateSelected",$poDateSelected);
  $smarty->assign("custIdSelected",$custIdSelected);
  $smarty->assign("custCodeSelected",$custCodeSelected);
  $smarty->assign("addressSelected",$addressSelected);
  $smarty->assign("contPersonSelected",$contPersonSelected);
  $smarty->assign("phNoSelected",$phNoSelected);
  $smarty->assign("remarksSelected",$remarksSelected);
  $smarty->assign("customerIdSelected",$customerIdSelected);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("address",$address);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("refGrnDetailId",$refGrnDetailId);
  $smarty->assign("srNo",$srNo);
  $smarty->assign("itemCode",$itemCode);
  $smarty->assign("range",$range);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("description",$description);
  $smarty->assign("expDelivDate",$expDelivDate);
  $smarty->assign("challan",$challan);
  $smarty->assign("received",$received);
  $smarty->assign("condition",$condition);
  $smarty->assign("parameterhidden",$parameterhidden);
  $smarty->assign("grnDetail",$grnDetail);
  $smarty->assign("grnCondition",$grnCondition);
  $smarty->display("grnEdit.tpl");
}
?>