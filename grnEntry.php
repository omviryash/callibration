<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnNo       = "";
  $grnPrefix   = "";
  $srNo        = "";
  $grnDate     = "";
  $itemCode    = "";
  $description = "";
  $challan     = "";
  $received    = "";
  $condition   = "";
  $useCustId   = "";
  $poNo        = "";
  $poDate      = "";
  $contPerson  = "";
  $phNo        = "";
  $poDate      = "";
  $remarks     = "";
  $expDelivDate= "";
  $customerId  = "";
  $custName    = "";
  $address     = "";
  $range       = "";
  $a           = 0;
  $insertvar   = "";
  $parameterId = array();
  $parameterName = array();
  $parameterhidden = array();

  if(isset($_POST['grnDateDay']))
  {
    if(isset($_POST['cancelBtn']))
    {
      header("Location: index.php");
      exit();
    }
    else if(isset($_POST['searchBtn']))
    {
      header("Location: grnList.php");
      exit();
    }
    
    if(isset($_POST['custName']) && strlen($_POST['custName']) > 0)
    {
      $insertCustQuery = "INSERT INTO customer(custName,custCode,address)
                           VALUE ('".$_POST['custName']."','".$_POST['custCode']."','".$_POST['address']."')";
      $insertCustQueryRes = mysql_query($insertCustQuery);
      if(!$insertCustQueryRes)
        die("Insert Query Not Inserted : ".mysql_error());

      $useCustId = mysql_insert_id();
    }
    else
      $useCustId      = ($_POST['customerId'] != '') ? $_POST['customerId'] : 0;
      
    $grnDate        =  $_POST['grnDateYear']."-".$_POST['grnDateMonth']."-".$_POST['grnDateDay'];
    $poDate         =  $_POST['poDateYear']."-".$_POST['poDateMonth']."-".$_POST['poDateDay'];
    $selectgrnentry = "INSERT INTO grnmaster(grnPrefix,grnNo,infoSheetNo,grnDate,poNo,customerId,poDate,mrAndMrs,contPerson,phNo,remarks,userName)
                        VALUE('".$_POST['grnPrefix']."','".$_POST['grnNo']."','".$_POST['infoSheetNo']."','".$grnDate."','".$_POST['poNo']."','".$useCustId."',
                              '".$poDate."','".$_POST['mrAndMrs']."','".$_POST['contPerson']."','".$_POST['phNo']."','".$_POST['remarks']."','".$_SESSION['s_activId']."')";
    $selectgrnentryResult = mysql_query($selectgrnentry);
    $grnId = mysql_insert_id();
    if(!$selectgrnentryResult)
      die("Insert Query Not Inserted 1: ".mysql_error(). " : ".$selectgrnentry  );
      
    $loopCount = 0;
    while($loopCount < count($_POST['itemId']))
    {
      $itemId        = ($_POST['itemId'][$loopCount] != '') ? $_POST['itemId'][$loopCount] : 0;
      $itemCode      = ($_POST['itemCode'][$loopCount] != '') ? $_POST['itemCode'][$loopCount] : 0;
      $parameterId   = ($_POST['parameterId'][$loopCount] != '') ? $_POST['parameterId'][$loopCount] : 0;
      $range         = ($_POST['range'][$loopCount] != '') ? $_POST['range'][$loopCount] : 0;
      $custReqDate   = ($_POST['custReqDate1'][$loopCount] != '') ? $_POST['custReqDate1'][$loopCount] : 0;
      $expDelivDate  = ($_POST['expDelivDate'][$loopCount] != '') ?  $_POST['expDelivDate'][$loopCount*3+2]['Year']."-".$_POST['expDelivDate'][$loopCount*3+1]['Month']."-".$_POST['expDelivDate'][$loopCount*3]['Day'] : 0;
      $description   = ($_POST['description'][$loopCount] != '') ? $_POST['description'][$loopCount] : "";
      $challan       = ($_POST['challang'][$loopCount] != '') ? $_POST['challang'][$loopCount] : 0;
      $received      = ($_POST['receivedg'][$loopCount] != '') ? $_POST['receivedg'][$loopCount] : 0;
      $condition     = ($_POST['condition'][$loopCount] != '') ? $_POST['condition'][$loopCount] : 0;
      if($_POST['itemId'][$loopCount] != "" || $_POST['itemCode'][$loopCount] != "" || $_POST['parameterId'][$loopCount] != "" || $_POST['range'][$loopCount] != "" || $_POST['description'][$loopCount] != "" ||
         $_POST['chllang'][$loopCount] != "" || $_POST['receivedg'][$loopCount] != "" || $_POST['condition'][$loopCount] != '')
      {
        $insertQueryGrnDetail = "INSERT INTO grndetail(grnId,itemId,itemCode,parameterId,rangeValue,custReqDate,expDelivDate,description,challan,received,grnCondition)
                                  VALUE (".$grnId.",".$itemId.",'".$itemCode."','".$parameterId."','".$range."','".$custReqDate."','".$expDelivDate."','".$description."','".$challan."','".$received."','".$condition."')";
        $insertQueryGrnDetailResult = mysql_query($insertQueryGrnDetail);
        $grnDetailId = mysql_insert_id();
      }
      $loopCount++;
    }
    if(isset($_POST['grnPrintBtn']))
      header("Location: grnPrintpdf.php?grnId=".$grnId);
    else if(isset($_POST['infoPrintBtn']))
      header("Location: grnPrintpdfInfo.php?grnId=".$grnId);
    else
      header("Location: grnEntry.php");
    exit();
  }

  /////////////////  Customer Name:Start
  $selectName = "SELECT customerId,custName,custCode,address
                   FROM customer
                  ORDER BY custName";
  $selectNameResult = mysql_query($selectName);
  while($nameRow = mysql_fetch_array($selectNameResult))
  {
    $customerId[$a] = $nameRow['customerId'];
    $custName[$a]   = $nameRow['custName'];
    $custCode[$a]    = $nameRow['custCode'];
    $address[$a]     = $nameRow['address'];
    $a++;
  }
  ///////////////// GRN Prefix Combo : Starts
  $b = 0;

  $selectgrnPrefix = "SELECT grnPrefixId,grnPrefix
                        FROM grnprefix
                       ORDER BY grnPrefix";
  $selectgrnPrefixResult = mysql_query($selectgrnPrefix);


  while($grnPrefixRow = mysql_fetch_array($selectgrnPrefixResult))
  {
    $grnPrefixId [$b]  = $grnPrefixRow['grnPrefixId'];
    $grnPrefix [$b]    = $grnPrefixRow['grnPrefix'];
    $b++;
  }
  ///////////////// GRN Prefix Combo : Ends
  
  ////////////////Info Sheet Maximum Value :Starts
  $selectInfoSheetNo = "SELECT (MAX(infoSheetNo)+1) AS maxInfoSheetNo
                       FROM grnmaster";
  $selectInfoSheetNoRes = mysql_query($selectInfoSheetNo);
  if ($selectInfoSheetNoRow = mysql_fetch_array($selectInfoSheetNoRes))
      $maxInfoSheetNo = $selectInfoSheetNoRow['maxInfoSheetNo'];
  ////////////////Info Sheet Maximum Value :Ends
  
  ///////////////// Item Name Combo : Starts
  $itemId = "";
  $itemName = "";
  $d = 0;

  $selectItem = "SELECT itemId,itemName
                   FROM item
                  ORDER BY itemName";
  $selectItemResult = mysql_query($selectItem);


  while($itemRow = mysql_fetch_array($selectItemResult))
  {
    $itemId [$d]  = $itemRow['itemId'];
    $itemName [$d]  = $itemRow['itemName'];
    $d++;
  }
  ///////////////// Item Name Combo : Ends
  
  ///////////////// parameter Name Combo : Starts
  $parameterId = "";
  $parameterName = "";
  $p = 0;
  $selectparameter = "SELECT parameterId,parameterName
                        FROM parameterentry
                       ORDER BY parameterName";
  $selectparameterResult = mysql_query($selectparameter);

  while($parameterRow = mysql_fetch_array($selectparameterResult))
  {
    $parameterId [$p]  = $parameterRow['parameterId'];
    $parameterName [$p]  = $parameterRow['parameterName'];
    $p++;
  }
  ///////////////// parameter Name Combo : Ends

  include("./bottom.php");
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("grnNo",$grnNo);
  $smarty->assign("maxInfoSheetNo",$maxInfoSheetNo);
  $smarty->assign("grnDate",$grnDate);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("custName",$custName);
  $smarty->assign("address",$address);
  $smarty->assign("poNo",$poNo);
  $smarty->assign("poDate",$poDate);
  $smarty->assign("contPerson",$contPerson);
  $smarty->assign("phNo",$phNo);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("srNo",$srNo);
  $smarty->assign("itemCode",$itemCode);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("range",$range);
  $smarty->assign("itemId",$itemId);
  $smarty->assign("itemName",$itemName);
  $smarty->assign("grnPrefix",$grnPrefix);
  $smarty->assign("parameterId",$parameterId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("description",$description);
  $smarty->assign("challan",$challan);
  $smarty->assign("received",$received);
  $smarty->assign("condition",$condition);
  $smarty->assign("customerId",$customerId);
  $smarty->assign("parameterName",$parameterName);
  $smarty->assign("parameterhidden",$parameterhidden);
  $smarty->display("grnEntry.tpl");
}
?>