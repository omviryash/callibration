<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $grnCount  = 0;
  $grnArray  = array();
  $msg       = "";
	$fromDate  = 0;
	$toDate    = 0;
	
	/////////////////  grnList date View:Start
	if(isset($_REQUEST['fromDateYear']))
		$fromDate  = $_REQUEST['fromDateYear']."-".$_REQUEST['fromDateMonth']."-".$_REQUEST['fromDateDay'];
	else
		$fromDate  = '2007-01-01';
	
	if(isset($_REQUEST['toDateYear']))
		$toDate    = $_REQUEST['toDateYear']."-".$_REQUEST['toDateMonth']."-".$_REQUEST['toDateDay'];
	else
		$toDate    = date("Y-m-d");
	/////////////////  grnList date View:Stop	
	
 $grnmasterQuery = "SELECT grnId,grnPrefix,grnNo,grnmaster.grnDate AS gDate, DATE_FORMAT(grnmaster.grnDate,'%d-%m-%y') AS grnDate,poNo,
                            DATE_FORMAT(grnmaster.poDate,'%d-%m-%y') AS poDate,custName,custCode,grnmaster.contPerson,phNo,
                            grnmaster.customerId,customer.customerId
                       FROM grnmaster
                       JOIN customer
                      WHERE grnmaster.customerId=customer.customerId
                        AND grnmaster.grnDate >= '".$fromDate."'
                        AND grnmaster.grnDate <= '".$toDate."'
                      ORDER BY cast(substr(grnNo,1) AS UNSIGNED)"; //grnId DESC";
  $grnmasterQueryResult = mysql_query($grnmasterQuery);
  
  while($grnListRow = mysql_fetch_array($grnmasterQueryResult))
  {
    $grnArray[$grnCount]['grnId']      = $grnListRow['grnId'];
    $grnArray[$grnCount]['grnPrefix']  = $grnListRow['grnPrefix'];
    $grnArray[$grnCount]['grnNo']      = $grnListRow['grnNo'];
    $grnArray[$grnCount]['grnDate']    = $grnListRow['grnDate'];
    $grnArray[$grnCount]['poNo']       = $grnListRow['poNo'];
    $grnArray[$grnCount]['poDate']     = $grnListRow['poDate'];
    $grnArray[$grnCount]['custName']   = $grnListRow['custName'];
    $grnArray[$grnCount]['custCode']   = $grnListRow['custCode'];
    $grnArray[$grnCount]['contPerson'] = $grnListRow['contPerson'];
    $grnArray[$grnCount]['phNo']       = $grnListRow['phNo'];
    $grnArray[$grnCount]['customerId'] = $grnListRow['customerId'];
    $grnCount++;
  }

  include("./bottom.php");
  $smarty->assign("msg",$msg);
	$smarty->assign("fromDate",'2007-01-01');
	$smarty->assign("toDate",$toDate);  
  $smarty->assign("grnArray",$grnArray);
  $smarty->assign("grnCount",$grnCount);
  $smarty->display("grnList.tpl");
}
?>