<?php /* Smarty version Smarty-3.0.6, created on 2015-01-10 18:19:37
         compiled from "./templates\-nablCertificateTable.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1620554b15f293674b7-22088883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2fc772ee0eb7cd5bb7f9d64036fe1278d1b37976' => 
    array (
      0 => './templates\\-nablCertificateTable.tpl',
      1 => 1418699652,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1620554b15f293674b7-22088883',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_counter')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.counter.php';
?><span style="float:right;margin:125px 0 0 0;"><b>QC F 05</b></span>
<div style="display:block;border:1px solid black;float:left;margin:0 0 0 55px;padding:5px;">
	<table border=0 cellpadding=0 cellspacing=3 width="100%" style="font:12px arial,sans-serif;">
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left">CERTIFICATE NO.</td>
			<td align="left">:</td>
			<td align="left"><?php echo $_smarty_tpl->getVariable('certificateNo')->value;?>
</td>
			<td align="left">&nbsp;</td>
			<td align="left">DATE OF CALLIBRATION</td>
			<td align="left">:</td>
			<td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['callibrationDate'];?>
</td>
			<td align="left" colspan="2">&nbsp;</td>
		</tr> 
		<tr>
      <td align="left" colspan="2">&nbsp;</td>
      <td align="left">NO OF SHEETS.</td>
			<td align="left">:</td>
			<td align="left">1 Of <?php echo $_smarty_tpl->getVariable('totalPages')->value;?>
</td>
			<td align="left">&nbsp;&nbsp;&nbsp;</td>
			<td align="left">Next Recommended Calibration date</td>
			<td align="left">:</td>
			<td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['nextYearDate'];?>
</td>
		  <td align="left" colspan="2">&nbsp;</td>
    </tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left">GRN No.</td>
			<td align="left">:</td>
			<td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['grnNo'];?>
</td>
			<td align="left" colspan="6">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>1.CUSTOMER NAME & ADDRESS</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4" style="font-family: Agency FB;font-size:18px;"><b>M/s. <?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['custName'];?>
</b></td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="6">&nbsp;</td>
			<td align="left" colspan="5" style="font-size:x-small;">
			  <?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['address'];?>

			  <br />
			  <b style="font-weight:bold;font-size:12px;"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['city'];?>
</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>2.CUSTOMERS REFERANCE NO.</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['custRefName'];?>
/cali/<?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['custCode'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>3.INSTRUMENT RECEIVED ON</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['grnDate'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="4"><b>4.DESCRIPTION OF INSTRUMENT</b></td>
			<td align="left" colspan="5">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['itemName'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make/Model.</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['makeModel'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Sr No.</td>
      <td align="left">:</td>
      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['itemCode'];?>
</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField1'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Instrument.ID.No.</td>
      <td align="left">:</td>
      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['instrumentId'];?>
</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField2'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Range.</td>
      <td align="left">:</td>
      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['range2'];?>
</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField3'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Least Count.</td>
      <td align="left">:</td>
      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['leastCount'];?>
</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField4'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Accuracy.</td>
      <td align="left">:</td>
      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['accuracy'];?>
</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField5'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Condition on receipt.</td>
      <td align="left">:</td>
      <td align="left">Good</td>
			<td align="left" colspan="4"><?php if ($_smarty_tpl->getVariable('grnDetailArr')->value['extraFields']==1){?><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['extraField6'];?>
<?php }else{ ?>&nbsp;<?php }?></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="8">
			  <table width="98%" border=0 cellpadding=1 cellspacing=0 style="font:12px arial,sans-serif;">
			  	<tr>
			  		<td align="left"><b>5.AMBIENT TEMPERATURE</b></td>
			  		<td align="left">:</td>
			  		<td align="left">&nbsp;<?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['temperature'];?>
</td>
						<td align="left">&nbsp;</td>
			      <td align="left"><b>RELATIVE HUMIDITY</b></td>
			      <td align="left">:</td>
			      <td align="left"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['humidity'];?>
</td>
			  </table>
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>6.DETAILS OF REFERANCE STANDARD & MAJOR INSTRUMENT USED.</b></td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="8">
			  <table width="98%" border=1 cellpadding=2 cellspacing=0 style="font-size:11px">
			    <tr>
			      <td align="center" style="white-space: nowrap"><b>Sr. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>Name & Make</b></td>
				  <!-- additional change by girish - start -->
			      <td align="center" style="white-space: nowrap"><b>Serial No.</b></td>
				  <!-- additional change by girish - end -->
			      <td align="center" style="white-space: nowrap"><b>Certi. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>I.D. No.</b></td>
			      <td align="center" style="white-space: nowrap"><b>Callibration Due Date</b></td>
			      <td align="center" style="white-space: nowrap"><b>Tracebility</b></td>
			    </tr>
	      <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['name'] = 'masterMeterSec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('masterMeterData')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['masterMeterSec']['total']);
?>
			    <tr>
			      <td align="center" width="20px"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['rownum'];?>
</td>
			      <td align="center" width="40px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterName'];?>
 (<?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterModelNo'];?>
)</td>
				  <!-- additional change by girish - start -->
			      <td align="center" width="20px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterSerialNo'];?>
</td>
				  <!-- additional change by girish - end -->
			      <td align="center" width="20px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterCertificateNo'];?>
</td>
			      <td align="center" width="10px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterIdNo'];?>
</td>
			      <td align="center" width="20px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterExp'];?>
</td>
			      <td align="center" width="20px" style="white-space: nowrap"><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[$_smarty_tpl->getVariable('smarty')->value['section']['masterMeterSec']['index']]['masterMeterTraceabilityTo'];?>
</td>
			    </tr>
			  <?php endfor; endif; ?>
			  </table> 
			</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
			<td align="left" colspan="9">
				<table width="98%" border=0 cellpadding=2 cellspacing=0 style="font-size:11px">
					<tr>
						<td align="left" width="4%"><b>Note: </b></td>
						<td align="left" width=""><?php echo $_smarty_tpl->getVariable('masterMeterData')->value[0]['masterUncertaintyText'];?>
</td>
					</tr>
				</table>
			</td>
		</tr>
<!--			
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Name</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterName'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;I.D No</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterIdNo'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Make</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterMake'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Model No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterModelNo'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Serial No.</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterSerialNo'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Certificate No</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterCertificateNo'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Calibration Valid Up To</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterExp'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3">&nbsp;&nbsp;&nbsp;Traceability To</td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterMeterTraceabilityTo'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
        &nbsp;&nbsp;&nbsp;Measurement uncertainty  <?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['masterUncertaintyText'];?>

      </td>
			<td align="left">&nbsp;</td>
		</tr>
-->	
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="3"><b>7.PROCEDURE</b></td>
      <td align="left">:</td>
      <td align="left" colspan="4"><?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['procedureText'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8"><b>Remarks</b> : <?php echo $_smarty_tpl->getVariable('grnDetailArr')->value['certiRemarks'];?>
</td>
			<td align="left">&nbsp;</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
		<td colspan="2">&nbsp;</td>
      <td align="left" colspan="10" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="4" style="font-size:x-small;">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      
      <td align="center" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
 		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="8" style="font-size:x-small;">
      	1.This report pertains To Partuculars sample/Instrumenst submitted for test.<br>
      	2.This certificate may not be reproduced except in full, without prior written permission of Krishna Instruments.<br>
      	3.This Calibraiton Result Reported in the Certificate are valid at the time of measurments and under stated Conditions.
      </td>
			<td align="left">&nbsp;</td>
		</tr>
  </table>
</div>
<div style="clear:both;">
</div>
<br/><br/>
<style>
@media print
{
.pagebreak { page-break-after:always; }
}
</style>
<div class="pagebreak"></div>
<?php $_template = new Smarty_Internal_Template('nablCertificateNewPageTop.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

		<tr>
			<td align="left" colspan="2">&nbsp;</td>
      <td align="left" colspan="9"><b>8.OBSERVATION</b></td>
		</tr>
		<tr>
		  <td colspan="11">
		  	<?php echo smarty_function_counter(array('assign'=>'currentpage','start'=>2,'print'=>false),$_smarty_tpl);?>

				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['name'] = 'sec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('masterArr')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total']);
?>
				  <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum']%$_smarty_tpl->getVariable('observationTableLimit')->value==0){?>
				    <?php $_template = new Smarty_Internal_Template('nablCertificateNewPageInclude.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
				  <?php }?>
				  <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['tableNo']==1){?>
				    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['dataPresent']==1){?>
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
                    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['displayCalculated']==1){?>
                      <td rowspan="2" align="center">Calculated<br /> Reading</td>
                    <?php }?>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
	              <tr>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec1']['rownum'];?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['testMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
                      <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['displayCalculated']==1){?>
                        <td align="center"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverageAfterMf'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
                      <?php }?>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorUnits'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorRdg'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['expandedUncertinityInPre'];?>
</td>
	              </tr>
	            <?php endfor; endif; ?>
	            </table>
	          <?php }?>
	        <?php }?>
				  <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['tableNo']==2){?>
				    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['dataPresent']==1){?>
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" rowspan="2">Calculated <br />Reading in <br />Volt</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Applied <br />Voltage</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
	              <tr>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec1']['rownum'];?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['testMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['tableNo2reading'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorUnitsTableNo2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorRdgTableNo2'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['expandedUncertinityInPre'];?>
</td>
	              </tr>
	            <?php endfor; endif; ?>
	            </table>
	          <?php }?>
	        <?php }?>
				  <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['tableNo']==3){?>
				    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['dataPresent']==1){?>
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" rowspan="2">Secondary<br /> Calculated<br /> Reading in<br /> Amps.</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Applied <br />Ampere</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Secondary <br />Reading<br />in Amps.</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
	              <tr>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec1']['rownum'];?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['testMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['tableNo3calculated'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorUnitsTableNo3'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorRdgTableNo3'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['expandedUncertinityInPre'];?>
</td>
	              </tr>
	            <?php endfor; endif; ?>
	            </table>
	          <?php }?>
	        <?php }?>
				  <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['tableNo']==4){?>
				    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['dataPresent']==1){?>
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" colspan="8">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Primary<br />Reading<br />in Volt.</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Secondary<br />Reading in<br />Volt.</td>
	              <td align="center">M.F.</td>
	              <td align="center">Calculated<br />Reading in<br />Volt.</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
	              <tr>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec1']['rownum'];?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['testMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['mf'];?>
</td>
	                <td align="center"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['tableNo4calculated'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorUnitsTableNo4'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorRdgTableNo4'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['expandedUncertinityInPre'];?>
</td>
	              </tr>
	            <?php endfor; endif; ?>
	            </table>
	          <?php }?>
	        <?php }?>
				  <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['tableNo']==5){?>
				    <?php if ($_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['dataPresent']==1){?>
				      <table border="1" cellspacing="0" style="font:12px arial,sans-serif;border:1px solid black;margin:10px;">
				      <tr>
				      	<div style="margin:10px;"><b><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</b></div>
			        </tr>
				      <tr>
				        <td align="left" colspan="2" rowspan="2">SR No</td>
	              <td align="center" colspan="6">Callibration Standard</td>
	              <td align="center" colspan="6">Unit Under Test</td>
	              <td align="center" colspan="6">Error</td>
	              <td align="left" colspan="2" rowspan="2">Expanded <br/>Uncert. In %</td>
	            </tr>
	            <tr>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Range</td>
	              <td align="center" colspan="3">Reading</td>
	              <td align="center" colspan="3">Units</td>
	              <td align="center" colspan="3">% of Rdg.</td>
	            </tr>
	            <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['name'] = 'sec1';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec1']['total']);
?>
	              <tr>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec1']['rownum'];?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['testMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo $_smarty_tpl->getVariable('masterArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range2'];?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeterAverage'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorUnits'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['stdMeter1AfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="3"><?php echo number_format($_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['errorRdg'],$_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['resolutionTypeAAfterDecimal'],".",'');?>
</td>
	                <td align="center" colspan="2"><?php echo $_smarty_tpl->getVariable('detailArr')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']][$_smarty_tpl->getVariable('smarty')->value['section']['sec1']['index']]['expandedUncertinityInPre'];?>
</td>
	              </tr>
	            <?php endfor; endif; ?>
	            </table>
	          <?php }?>
	        <?php }?>
	      <?php endfor; endif; ?>
	    </td>
		</tr>

<?php $_template = new Smarty_Internal_Template('nablCertificateNewPageBottom.tpl', $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
