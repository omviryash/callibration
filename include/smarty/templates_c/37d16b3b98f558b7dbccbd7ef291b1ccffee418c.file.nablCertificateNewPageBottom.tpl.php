<?php /* Smarty version Smarty-3.0.6, created on 2015-01-10 18:19:37
         compiled from "./templates\nablCertificateNewPageBottom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2433954b15f29e60998-47268241%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37d16b3b98f558b7dbccbd7ef291b1ccffee418c' => 
    array (
      0 => './templates\\nablCertificateNewPageBottom.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2433954b15f29e60998-47268241',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
		<tr>
			<td colspan="9">
			  <table border="0" cellspacing="0" style="border:0px solid black;margin-left:50px;font-size:x-small;font:12px arial,sans-serif;">
			  <tr><td colspan="2"><b>NOTE :</b></td></tr>
			  <tr>
			    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			    <td style="font-size:x-small;">
			      1. The reported expanded uncertainty in measurement is stated as the standard uncertainty <br/>
			      in measurement multiplied by the coverage factor k=2, which for a normal distribution <br/>
			      corresponds to a coverage probability of approximately 95%.
			    </td>
			  </tr>
			  </table>
			</td>
		</tr>
		<tr><td colspan="11"><hr style="border:1px solid #000;"></td></tr>
		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="10" style="font-family: 'Exotc350 DmBd BT';font-size:20px; "><b>For Krishna Instruments</b></td>
		</tr>
		<tr><td align="left" colspan="11">&nbsp;</td></tr>
 		<tr>
			<td align="left" colspan="1">&nbsp;</td>
      <td align="left" colspan="4">CALIBRATED BY : (Bhavesh Tank, CAL. ENG.)</td>
      <td align="left">&nbsp;</td>
      <td align="left" colspan="3">APPROVED BY : (D.R.SHAH, C.E.O.)</td>
			<td align="left" colspan="2">SEAL:</td>
		</tr>
  </table>
</div>
</div>
