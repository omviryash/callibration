<?php /* Smarty version Smarty-3.0.6, created on 2015-01-09 14:05:08
         compiled from "./templates\observationList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:681954afd2047d5299-69639139%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4448356b9058f774ca42180008c19bb8abfb8348' => 
    array (
      0 => './templates\\observationList.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '681954afd2047d5299-69639139',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_options.php';
?><?php $_template = new Smarty_Internal_Template("./headStart.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<script type="text/javascript">
$(document).ready(function() {
  $('#grnId').focus();
  $("#grnId").change(function(){
    var datastring = "grnId=" + $('#grnId').val();
    $.ajax({
      url: "./observationGrnAj.php",
      data: datastring,
      success: function(data){
        $('#instrument').html(data);
        $("#grnDetailId").change(function(){
          getDetails();
        });
      }
    });
  });
});
function getDetails()
{
  var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val();
	$.ajax({
	  url: "./obsDetailAj.php",
	  data: datastring,
	  success: function(response){
	    $('#obsDetail').html(response);
	  }
	});
}

function deleteObs(grnObsId)
{
	var answer = confirm("Are You Sure Delete Your Recored ?")
	if (answer == true)
	{
	  var datastring = "grnObservationId=" + grnObsId;
		$.ajax({
		  url: "./obsDetailDelete.php",
		  data: datastring,
		  method: 'POST',
		  success: function(response){
		    getDetails();
		  }
		});
  }
	return false;
}
</script>
<?php $_template = new Smarty_Internal_Template("./headEnd.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<center class="center"><h2>Observation  List</h2></center><br>
<tr>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Customer Id</td>
</tr>
<tr>
  <td>
    <select name="grnId" id="grnId">
      <option value="0">GRN List</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('grnId')->value,'output'=>$_smarty_tpl->getVariable('grnPrefixNo')->value),$_smarty_tpl);?>

    </select>
  </td>
  <td>
    <span id="instrument">
      <select name="grnDetailId" id="grnDetailId">
        <option>Select Instrument</option>
      </select>
    </span>
  </td>
</tr>
</table>
<div id="obsDetail"></div>