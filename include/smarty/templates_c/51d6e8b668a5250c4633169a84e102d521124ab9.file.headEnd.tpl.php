<?php /* Smarty version Smarty-3.0.6, created on 2015-01-09 14:04:59
         compiled from "./templates\./headEnd.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2616454afd1fb40ff40-66216704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '51d6e8b668a5250c4633169a84e102d521124ab9' => 
    array (
      0 => './templates\\./headEnd.tpl',
      1 => 1415987856,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2616454afd1fb40ff40-66216704',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (strlen($_smarty_tpl->getVariable('s_activId')->value)>0){?>
<script type="text/javascript">
//SuckerTree Horizontal Menu (Sept 14th, 06)
//By Dynamic Drive: http://www.dynamicdrive.com/style/
var menuids=["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus_horizontal()
{
  for (var i=0; i<menuids.length; i++)
  {
    var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++)
    {
  		if(ultags[t].parentNode.parentNode.id==menuids[i])
  		{ //if this is a first level submenu
  			ultags[t].style.top=ultags[t].parentNode.offsetHeight+"px" //dynamically position first level submenus to be height of main menu item
  			ultags[t].parentNode.getElementsByTagName("a")[0].className="mainfoldericon"
		  }
		  else
		  { //else if this is a sub level menu (ul)
  		  ultags[t].style.left=ultags[t-1].getElementsByTagName("a")[0].offsetWidth+"px" //position menu to the right of menu item that activated it
      	ultags[t].parentNode.getElementsByTagName("a")[0].className="subfoldericon"
		  }
      ultags[t].parentNode.onmouseover=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="visible"
      }
      ultags[t].parentNode.onmouseout=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="hidden"
      }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus_horizontal, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus_horizontal)

</script>
<?php }?>
</head>
<body>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="content">
    <?php if (strlen($_smarty_tpl->getVariable('s_activId')->value)>0){?>
      <a href="logout.php" class="link">Logout</a> |
      <b>User Name : <?php echo $_smarty_tpl->getVariable('s_activId')->value;?>
</b>
      <span style="float:right"><a href="backup.php" class="link">Backup</a> </span>
    <?php }else{ ?>
      <a href="checkLogin.php" class="link">Login</a>
    <?php }?>
    <br /><br />
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <?php if (strlen($_smarty_tpl->getVariable('s_activId')->value)>0){?>
        <tr>
          <td class="bluebg1">
            <div class="suckertreemenu">
            <br />
            <ul id="treemenu1">
              <li><a href="#">Master</a>
                <ul>
                  <li><a href="grnPrefix.php" class="link">GRN Prefix</a></li>
                  <li><a href="item.php" class="link">Item</a></li>
                  <li><a href="parameterEntry.php" class="link">Parameter</a></li>
                  <li><a href="masterMeter.php" class="link">Master Meter</a></li>
                  <li><a href="masterMeterSub.php" class="link">Master Meter Sub</a></li>
                  <li><a href="#" class="link">Customer</a>
                    <ul style="padding-left: 0px;">
                      <li><a href="custEntry.php" class="link">Entry</a></li>
                      <li><a href="custList.php" class="link">List</a></li>
                    </ul>
                  </li>
                  <li><a href="symbols.php" class="link" target="_blank">SYMBOLS</a></li>
                </ul>
              </li>
              <li><a href="#" class="link">G.R.N.</a>
                <ul>
                  <li><a href="grnEntry.php" class="link">Entry</a></li>
                  <li><a href="grnList.php" class="link">List</a></li>
                  <li><a href="grnItemList.php" class="link">Item List</a></li>
                </ul>
              </li>
              <?php if (strlen($_smarty_tpl->getVariable('s_userType')->value)>0&&$_smarty_tpl->getVariable('s_userType')->value=="Admin"){?>
                <li><a href="staffEntry.php" class="link">Staff Entry</a></li>
              <?php }?>
              <li><a href="#" class="link">Observation Sheet</a>
                <ul>
                  <li><a href="observationSheet.php" class="link">NABL ENTRY</a></li>
                  <!--li><a href="observationSheetRegular.php" class="link">REGULAR ENTRY</a></li-->
                  <li><a href="observationList.php" class="link">List</a></li>
                  <li><a href="observationPrint.php" class="link">Print</a></li>
                  <li><a href="calUncertainty.php" class="link">Calculation of Uncertainty</a></li>
                  <li><a href="listOfCertificate.php" class="link">List of Certificate</a></li>
				  <!-- additional change by girish - start -->
				  <li><a href="instrumentsDues.php" class="link">Instruments Dues</a></li>
				  <!-- additional change by girish - end -->
                </ul>
              </li>
              <li><a href="deliveryChallan.php" class="link">Delivery Challan</a></li>
            </ul>
            <br /><br /><br />
            </div>
          </td>
        </tr>
        <?php }?>
        <tr>
          <td class="content">
<br /><br />
	