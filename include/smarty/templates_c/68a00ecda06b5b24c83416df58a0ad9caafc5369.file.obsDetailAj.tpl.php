<?php /* Smarty version Smarty-3.0.6, created on 2015-01-09 14:05:12
         compiled from "./templates\obsDetailAj.tpl" */ ?>
<?php /*%%SmartyHeaderCode:444154afd2083d9d28-69603198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '68a00ecda06b5b24c83416df58a0ad9caafc5369' => 
    array (
      0 => './templates\\obsDetailAj.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '444154afd2083d9d28-69603198',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<table align='center' border='0' cellpadding='1' cellspacing='2'>
<?php if ($_smarty_tpl->getVariable('recordNotFound')->value==''){?>
<center class="center"><h2>Measurement!</h2></center><br>
<?php }else{ ?>
<center><h1 style="color:red;"><?php echo $_smarty_tpl->getVariable('recordNotFound')->value;?>
</h1></center><br>
<?php }?>

  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('stdMeter1')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td class="table1" align='center'>Std. Meter Value</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter3')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter4')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter5')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeterAverage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <!--td class="table2" align="right"><a href="./uncertaintyBudget.php?grnObservationId=<?php echo $_smarty_tpl->getVariable('grnObservationId')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
">Uncertainty Budget</a></td>
    <td class="table2" align="right"><a href="./kcallibrationCertificate.php?grnId=<?php echo $_smarty_tpl->getVariable('grnId')->value;?>
&grnDetailId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">Regular Certificate</a></td-->
    <td class="table2" align="right"><!--a href="./nablCertificate2.php?grnId=<?php echo $_smarty_tpl->getVariable('grnId')->value;?>
&grnDetailId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">NABL Certificate</a--></td>
    <td class="table2" align="right"><a href="./nablCertificateTable.php?grnId=<?php echo $_smarty_tpl->getVariable('grnId')->value;?>
&grnDetailId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">NABL Certificate</a></td>
    <td class="table2" align="right"><input type="button" value="DELETE" onclick="deleteObs(<?php echo $_smarty_tpl->getVariable('grnObservationId')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
);" /></td>
    <td class="table2" align="right"><a href="./observationSheetEdit.php?grnObservationId=<?php echo $_smarty_tpl->getVariable('grnObservationId')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
&grnObsMasterId=<?php echo $_smarty_tpl->getVariable('grnObsMasterId')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
">Edit</a></td>
    <!--a href="./nablCertificate2.php?grnId=<?php echo $_smarty_tpl->getVariable('grnId')->value;?>
&grnDetailId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">Delete</a-->
    <!--td class="table2" align="right"><a href="./observationSheetRegular.php?grnDetailPassId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">Test More Parameter</a></td>
    <td class="table2" align="right"><a href="./observationSheet.php?grnDetailPassId=<?php echo $_smarty_tpl->getVariable('grnDetailId')->value;?>
">Test More Parameter For Nabl</a></td-->
    
  </tr>
  <tr>
    <td  class="table1" align='center'>Test Meter Value</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter3')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter4')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter5')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeterAverage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
  </tr><tr></tr><tr></tr><tr></tr>
  
  <?php endfor; endif; ?>
</table>