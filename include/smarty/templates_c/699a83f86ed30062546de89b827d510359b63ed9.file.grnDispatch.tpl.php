<?php /* Smarty version Smarty-3.0.6, created on 2015-02-02 19:21:23
         compiled from "./templates\grnDispatch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:866154cfc023346259-13381333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '699a83f86ed30062546de89b827d510359b63ed9' => 
    array (
      0 => './templates\\grnDispatch.tpl',
      1 => 1422901272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '866154cfc023346259-13381333',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_select_date.php';
?><?php $_template = new Smarty_Internal_Template("./headStart.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<link type="text/css" href="./css/maincombobox.css" />
<script type="text/javascript">
  $(document).ready(function(){
    $('#grnPrefix').focus();
    customerIdFunc();
    $('#custName').attr("disabled", true);
    $('#custCode').attr("disabled", true);
    $('#address').attr("disabled", true);
    $(document).keydown(function(e) {
    	var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 13){
        window.event.keyCode = 9;
      }
    });
    $('#grnPrefix').change(function(){
      var grnPrefix = $("#grnPrefix").val();
      var datastring = 'grnPrefix='+ grnPrefix;
      $.ajax({
        url: "grnEntryJq.php",
        data: datastring,
        success: function(data){
          $("#grnNo").val(data);
        }
      });
    });

	  $("#customerId").change(function ()
	  {
	    if($("#customerId").val() > 0)
	    {
	      $('#custName').val("");
	      $('#custName').attr("disabled", true);
	      $('#custCode').attr("disabled", true);
	      $('#address').attr("disabled", true);
	    }
	    else
	    {
	      $('#custName').removeAttr("disabled");
	      $('#custCode').removeAttr("disabled");
	      $('#address').removeAttr("disabled");
	    }
	  });
  });

  function customerIdFunc(){
    var customerId = $("#customerId").val();
    var datastring = 'customerId=' + customerId;
    $.ajax({
          url: "customerCodeJq.php",
          data: datastring,
          success: function(data){
            $("#custCode").val(data);
          }
    });
  }
//validation function----------> Start
(function($,W,D)
{
  var JQUERY4U = {};
  JQUERY4U.UTIL =
  {
    setupFormValidation: function()
    {
    //form validation rules
    $("#form1").validate({
    rules: {
      deliveryMode: {
          required: true
      },
    },
    messages: {
      deliveryMode: {
          required: '* Please provide Delivery mode',
      },
    },
      submitHandler: function(form) {
        form.submit();
      }
      });
    }
  }
  //when the dom has loaded setup form validation rules
  $(D).ready(function($) {
      JQUERY4U.UTIL.setupFormValidation();
  });
  
})(jQuery, window, document);
//validation function----------> End
</script>
<script>
  $('#parameterId').val(function() {
    return val + (!val ? '' : ', ') + 'parameterId';
      alert("val")
  });
</script>
<?php $_template = new Smarty_Internal_Template("./headEnd.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<form name="form1" id="form1" method="post">
<div>
<input type="hidden" name="paramhidden" id="paramhidden">        
<input type="hidden" name="gatePassId" size="5" value="<?php echo $_smarty_tpl->getVariable('gatePassMaxId')->value;?>
">     
<table border="0" align="center">
  <tr>
    <td>GRN No:
      <select name="grnPrefix" id="grnPrefix" >
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('grnPrefix')->value,'output'=>$_smarty_tpl->getVariable('grnPrefix')->value,'selected'=>$_smarty_tpl->getVariable('grnPrefixSelected')->value),$_smarty_tpl);?>

      </select>
    </td>
    <td><input type="text" name="grnNo" id="grnNo" value="<?php echo $_smarty_tpl->getVariable('grnNoSelected')->value;?>
"></td>
    <td>Info Sheet No.:
      <input type="text" name="infoSheetNo" value=<?php echo $_smarty_tpl->getVariable('infoSheetNo')->value;?>
 id="srNoClass" size="3" />
    </td>
    <td>Date: </td>
    <td NOWRAP>
      <?php echo smarty_function_html_select_date(array('prefix'=>"grnDate",'start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td>P.O. / Letter no: </td><td> <input value="<?php echo $_smarty_tpl->getVariable('poNoSelected')->value;?>
" type="text" name="poNo" id="poNo"></td>
    <td> PO Date :</td>
     <td NOWRAP>
      <?php echo smarty_function_html_select_date(array('time'=>$_smarty_tpl->getVariable('poDateSelected')->value,'prefix'=>"poDate",'start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
  </tr>
  <tr>
  <tr>
    <td>Customer Name :</td>
    <td colspan="3">
      <select name="customerId" id="customerId" onchange="customerIdFunc();">
        <option value='0'>Select Name</option>
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('customerId')->value,'output'=>$_smarty_tpl->getVariable('custName')->value,'selected'=>$_smarty_tpl->getVariable('customerIdSelected')->value),$_smarty_tpl);?>

      </select><br />
      <input type="text" name="custName" id="custName" size=55>
    </td>
    <td>Code :</td><td><input type="text" value="<?php echo $_smarty_tpl->getVariable('custCodeSelected')->value;?>
" name="custCode" id="custCode"></td>
  </tr>
  <tr>
    <td>Address :</td>
    <td><textarea rows="3" cols="25" name="address" id="address"><?php echo $_smarty_tpl->getVariable('addressSelected')->value;?>
</textarea>
    </td>
  </tr>
  <tr>
    <td>Contact Person :</td>
    <td colspan="3"><input type="text" name="contPerson" value="<?php echo $_smarty_tpl->getVariable('contPersonSelected')->value;?>
" id="contPerson" size=55></td>
    <td>Ph No :</td>
    <td><input type="text" name="phNo" value="<?php echo $_smarty_tpl->getVariable('phNoSelected')->value;?>
" id="phNo"></td>
  </tr>
  <tr>
    <td>Remarks :</td>
    <td colspan="14"><input type="text" name="remarks" value="<?php echo $_smarty_tpl->getVariable('remarksSelected')->value;?>
" id="remarks" size=155>
  </tr>
</table>
</div>

<center class="center"><h2>Following Materials Received For - Calibration/Verification</h2></center><br>
<table align="center" border="1"  cellpadding="1" cellspacing="0">

<tr>
<td colspan="13">
<table align="center" border="0" id="dataTable" width="100%" cellpadding="1" cellspacing="0">
 <tr>
    <th align="center" rowspan="2" width="100%" style="width :135px"> SR. NO. </th>
    <th align="center" rowspan="2" style="width :150px"> ITEM. Description </th>
    <th align="center" rowspan="2" style="width :135px"> ID. CODE. </th>
    <th align="center" rowspan="2" style="width :147px"> Paramter </th>
    <th align="center" rowspan="2"> Range </th>
    <th align="center" rowspan="2" style="width :150px"> Cali. Due Date Requested By Cust.</th>
    <th align="center" rowspan="2" style="width :125px">Expected Delivery Date</th>
    <th colspan="3" align="center" > Q U A N T I T Y</th>
    <th align="center" rowspan="2" style="width :120px"> Notes </th>
    <th align="center" rowspan="2" style="width :120px"> callibration Report.No &nbsp;</th>
  </tr>
  <tr>
    <th align="center" style="width :130px"> Challan </th>
    <th align="center" style="width :130px"> Recd. </th>
    <th align="center" style="width :130px"> Condition </th>
  </tr>
  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('grnDetail')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td align="center" NOWRAP>
      <input type="checkbox" name="dispatch[<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnDetailId'];?>
]" value="1" />
      <input type="hidden" name="grnDetailId[<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnDetailId'];?>
]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnDetailId'];?>
"/>
      <input type="text" name="srNo[]" value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum'];?>
" class="srNoClass" size="3" DISABLED /></td>
    <td align="center">
      <select name="itemId[]" style="width: 148px;">
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('itemId')->value,'output'=>$_smarty_tpl->getVariable('itemName')->value,'selected'=>$_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['itemId']),$_smarty_tpl);?>

      </select>
    </td>
    <td align="center" ><input type="text" name="itemCode[]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['itemCode'];?>
" /></td>
    <td>
      <select name="parameterId[]" id="parameterId">
        <?php echo smarty_function_html_options(array('values'=>($_smarty_tpl->getVariable('parameterId')->value),'output'=>($_smarty_tpl->getVariable('parameterName')->value),'selected'=>$_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterId']),$_smarty_tpl);?>

      </select><br />
    </td>
    <td>
      <input type="text" name="range[]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
" />
    </td>
    <td class="table2" nowrap>
      <input type="text" name="custReqDate1[]" id="custReqDate1[]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['custReqDate'];?>
" >
    </td>
    <td class="table2" nowrap>
      <?php echo smarty_function_html_select_date(array('field_array'=>"expDelivDate[]",'time'=>$_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['expDelivDate'],'prefix'=>'','start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td align="center" ><input type="text" name="challang[]" size="5" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['challan'];?>
" /></td>
    <td align="center" ><input type="text" name="receivedg[]" size="5" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['received'];?>
" class="recdClass" /></td>
    <td>
      <select name="condition[]" style="width :135px" value=$grnDetail[sec].grnCondition onchange="document.infoSheet.submit();">
        <?php echo smarty_function_html_options(array('values'=>($_smarty_tpl->getVariable('grnCondition')->value),'output'=>($_smarty_tpl->getVariable('grnCondition')->value),'selected'=>$_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnCondition']),$_smarty_tpl);?>

      </select>
    </td>
    <td align="center" ><input type="text" name="description[]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['description'];?>
" /></td>
    <td align="center" ><input type="text" name="reportNo[<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnDetailId'];?>
]" value="<?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['reportNo'];?>
" /></td>
  </tr>
  <?php endfor; endif; ?>
  <tr>
    <td align="right" colspan="2">Mode of Delivery : </td>
    <td align="left" colspan="3"><input type="text" name="deliveryMode" size="15" /></td>
  </tr>
</tr>
</table>
<table align="center">
  <tr>
    <td colspan=9">
      <input type="submit" value="SUBMIT" name="submitBtn" />
      <input type="submit" value="GRN Print (Old Address)" name="grnPrintBtn" />
	  <input type="submit" value="GRN Print (New Address)" name="grnPrintNewAddrBtn" />
      <input type="submit" value="INFO Print (Old Address)" name="infoPrintBtn" />
	  <input type="submit" value="INFO Print (New Address)" name="infoPrintNewAddrBtn" />
      <input type="submit" value="SEARCH" name="searchBtn" />
      <input type="submit" value="CANCEL" name="cancelBtn" />
    </td>
  </tr>
</table>
</form>
<?php $_template = new Smarty_Internal_Template("footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>