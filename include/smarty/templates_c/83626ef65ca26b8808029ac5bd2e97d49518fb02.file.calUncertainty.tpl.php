<?php /* Smarty version Smarty-3.0.6, created on 2015-01-11 16:41:59
         compiled from "./templates\calUncertainty.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1739854b299c7df7841-66810288%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '83626ef65ca26b8808029ac5bd2e97d49518fb02' => 
    array (
      0 => './templates\\calUncertainty.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1739854b299c7df7841-66810288',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_options.php';
?><?php $_template = new Smarty_Internal_Template("./headStart.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<script type="text/javascript">
$(document).ready(function() {
  $('#grnId').focus();
  $("#grnId").change(function(){
    var datastring = "grnId=" + $('#grnId').val();
    $.ajax({
      url: "./observationGrnAj.php",
      data: datastring,
      success: function(data){
        $('#instrument').html(data);
        $("#grnDetailId").change(function(){
          var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val();
          $.ajax({
            url: "./calUncerDetailAj.php",
            data: datastring,
            success: function(response){
              $('#obsDetail').html(response);
              $('#grnIdAj').val($('#grnId').val());
              $('#grnDetailIdAj').val($('#grnDetailId').val());
              $('input[name=buttonPrint]').hide();
              $('#printBtn').append('<input type="submit" name="buttonPrint" value=" P R I N T " />');
            }
          });
        });
      }
    });
  });
});
</script>
<?php $_template = new Smarty_Internal_Template("./headEnd.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<form name="printData" id="printData" method="get" action="calUncerDetailAj.php">
<input type="hidden" name="grnId" id="grnIdAj" />
<input type="hidden" name="grnDetailId" id="grnDetailIdAj" />
<span id="printBtn"></span>
</form>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Customer Id</td>
</tr>
<tr>
  <td>
    <select name="grnId" id="grnId">
      <option value="0">GRN List</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('grnId')->value,'output'=>$_smarty_tpl->getVariable('grnPrefixNo')->value),$_smarty_tpl);?>

    </select>
  </td>
  <td>
    <span id="instrument">
      <select name="grnDetailId" id="grnDetailId">
        <option>Select Instrument</option>
      </select>
    </span>
  </td>
</tr>
</table>
<br/>
<div id="obsDetail"></div>