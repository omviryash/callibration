<?php /* Smarty version Smarty-3.0.6, created on 2015-01-10 18:22:32
         compiled from "./templates\grnEntry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3222654b15fd8d8b305-62998861%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa1b2f6ee24b333b2dae629a3015b5250025f40e' => 
    array (
      0 => './templates\\grnEntry.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3222654b15fd8d8b305-62998861',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_function_html_options')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'C:\xampp\htdocs\work\vipulshah\kcallibration1\include\smarty\libs\plugins\function.html_select_date.php';
?><?php $_template = new Smarty_Internal_Template("./headStart.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<link type="text/css" href="./css/maincombobox.css" />
<script type="text/javascript">
  $(document).ready(function(){
    $('#grnPrefix').focus();
    customerIdFunc();
    $(document).keydown(function(e) {
    	var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 13){
        window.event.keyCode = 9;
      }
    });
    $('#grnPrefix').change(function(){
      var grnPrefix = $("#grnPrefix").val();
      var datastring = 'grnPrefix='+ grnPrefix;
      $.ajax({
        url: "grnEntryJq.php",
        data: datastring,
        success: function(data){
          $("#grnNo").val(data);
        }
      });
    });

	  $("#customerId").change(function ()
	  {
	    if($("#customerId").val() > 0)
	    {
	      $('#custName').attr("disabled", true);
	      $('#custCode').attr("disabled", true);
	      $('#address').attr("disabled", true);
	    }
	    else
	    {
	      $('#custName').removeAttr("disabled");
	      $('#custCode').removeAttr("disabled");
	      $('#address').removeAttr("disabled");
	    }
	  });
	  
	  $("#grnNo").blur(function(){
		var getVal=$("#grnNo").val();
		$("#srNoClass").val(getVal);
	  });
  });

  function addRow(tableID)
  {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var colCount = table.rows[2].cells.length;

    for(var i=0; i<colCount; i++)
    {
      var newcell = row.insertCell(i);
      newcell.innerHTML = table.rows[2].cells[i].innerHTML;
      //alert(newcell.childNodes);
      switch(newcell.childNodes[0].type)
      {
        case "text":
          if(i==0)
            newcell.childNodes[0].value = rowCount-1;
          else
          {
            if(newcell.childNodes[0].className == "recdClass")
              newcell.childNodes[0].value = 1;
            else
              newcell.childNodes[0].value = "";
          }
          break;
      }
    }
  }

  function deleteRow() {
     var countSr = 1;
     $('.delete').click(function(){
        $(this).parent().parent().remove();
        $('.srNoClass').each(function(){
          this.value = countSr++;
        });
        return false;
     });
  }
  function customerIdFunc(){
    var customerId = $("#customerId").val();
    var datastring = 'customerId=' + customerId;
    $.ajax({
          url: "customerCodeJq.php",
          data: datastring,
          dataType:'json',
          success: function(data){
            $("#custCode").val(data[0]);
            $("#address").val(data[1]);
            $("#custName").val(data[2]);
            $("#contPerson").val(data[3]);
            $("#phNo").val(data[4]);
          }
    });
  }
</script>
<script>
  $('#parameterId').val(function() {
    return val + (!val ? '' : ', ') + 'parameterId';
      alert("val")
  });
</script>
<?php $_template = new Smarty_Internal_Template("./headEnd.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<form name="form1" id="form1" method="post">
<BODY onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
<div>
<input type="hidden" name="paramhidden" id="paramhidden">
<table border="0" align="center">
  <tr>
    <td>GRN No:
      <select name="grnPrefix" id="grnPrefix">
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('grnPrefix')->value,'output'=>$_smarty_tpl->getVariable('grnPrefix')->value),$_smarty_tpl);?>

      </select>
    </td>
    <td><input type="text" name="grnNo" id="grnNo" required="required" title="GrnNo"></td>
    <td>Info Sheet No.:
      <input type="text" name="infoSheetNo" value="<?php echo $_smarty_tpl->getVariable('maxInfoSheetNo')->value;?>
" readonly id="srNoClass" size="3" required="required" title="InfoSheetNo"/>
    </td>
    <td>Date: </td>
    <td NOWRAP>
      <?php echo smarty_function_html_select_date(array('prefix'=>"grnDate",'time'=>($_smarty_tpl->getVariable('grnDate')->value),'start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td>P.O. / Letter no: </td><td> <input type="text" name="poNo" id="poNo"  required="required" title="PO /LetterNo"></td>
    <td> PO Date :</td>
     <td NOWRAP>
      <?php echo smarty_function_html_select_date(array('prefix'=>"poDate",'time'=>($_smarty_tpl->getVariable('poDate')->value),'start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
  </tr>
  <tr>
  <tr>
    <td>Customer Name :</td>
    <td colspan="4">
      <select name="customerId" id="customerId" onchange="customerIdFunc();">
        <option value='0'>Select Name</option>
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('customerId')->value,'output'=>$_smarty_tpl->getVariable('custName')->value),$_smarty_tpl);?>

      </select><br />
      <input type="text" name="custName" id="custName" size=58>
    </td>
    <td>Code :</td><td><input type="text" name="custCode" id="custCode"></td>
  </tr>
  <tr>
    <td>Address :</td>
    <td><textarea rows="3" cols="25" name="address" id="address"></textarea>
    </td>
  </tr>
  <tr>
    <td>Contact Person :</td>
    <td colspan="4">
    	<select name="mrAndMrs">
    	  <option value="Mr">Mr</option>
    	  <option value="Mrs">Mrs</option>
    	</select>
    	<input type="text" name="contPerson" id="contPerson" size=58  required="required" title="Contact Person"></td>
    <td>Ph No :</td>
    <td><input type="text" name="phNo" id="phNo"  required="required" title="Phone Number"></td>
  </tr>
  <tr>
    <td>Remarks :</td>
    <td colspan="14"><input type="text" name="remarks" id="remarks" size=155  required="required" title="Remarks">
  </tr>
</table>
</div>

<center class="center"><h2>Following Materials Received For - Calibration/Verification</h2></center><br>
<table align="center" border="1"  cellpadding="1" cellspacing="0">
<tr>
<td colspan="13">
<table align="center" border="0" id="dataTable" width="100%" cellpadding="1" cellspacing="0">
 <tr>
    <th align="center" rowspan="2" style="width :135px"> SR. NO. </th>
    <th align="center" rowspan="2" style="width :150px"> ITEM. Description </th>
    <th align="center" rowspan="2" style="width :135px"> ID. CODE. </th>
    <th align="center" rowspan="2" style="width :147px"> Paramter </th>
    <th align="center" rowspan="2"> Range </th>
    <th align="center" rowspan="2"> Cali. Due Date Requested By Cust.</th>
    <th align="center" rowspan="2">Expected Delivery<br> Date</th>
    <th align="center" colspan="3" > Q U A N T I T Y</th>
    <th align="center" rowspan="2" style="width :120px"> Notes </th>
    <th align="center" rowspan="2" style="width :30px">Add Row</th>
    <th align="center" rowspan="2" style="width :30px">Delete Row</th>
  </tr>
  <tr>
    <th align="center" style="width :130px"> Challan </th>
    <th align="center" style="width :130px"> Recd. </th>
     <th align="center" style="width :130px"> Condition </th>

  </tr>
 <tr>
    <td ><input type="text" name="srNo[]" value="1" class="srNoClass" size="1" DISABLED /></td>
    <td ><select name="itemId[]" style="width :145px">
        <option>Select Item Name</option>
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->getVariable('itemId')->value,'output'=>$_smarty_tpl->getVariable('itemName')->value),$_smarty_tpl);?>

      </select></td>
    <td ><input type="text" name="itemCode[]" size="8"  required="required" title="Item Code"/></td>
    <td>
        <select name="parameterId[]" id="parameterId">
        <?php echo smarty_function_html_options(array('values'=>($_smarty_tpl->getVariable('parameterId')->value),'output'=>($_smarty_tpl->getVariable('parameterName')->value)),$_smarty_tpl);?>

      </select><br />
    </td>
    <td>
      <input type="text" name="range[]" size="10"  required="required" title="Range" />
    </td>
    <td nowrap>
      <input type="text" name="custReqDate1[]" id="custReqDate1[]" value="1 Year" size="4" >
    </td>
    <td nowrap>
      <?php echo smarty_function_html_select_date(array('field_array'=>"expDelivDate[]",'prefix'=>'','start_year'=>"-3",'end_year'=>"+1",'month_format'=>"%m",'field_order'=>"DMY",'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td><input type="text" name="challang[]" size="1" value="1"  required="required" title="Challan"/></td>
    <td ><input type="text" name="receivedg[]" size="1" value="1" class="recdClass" /></td>
    <td>
      <select name="condition[]" style="width :98px" onchange="document.infoSheet.submit();">
      <option>Ok</option>
      <option>Faulty</option>
      <option>NotChecked</option>
      </select>
    </td>
    <td align="center" ><input type="text" name="description[]" /></td>
    <td><input type="button" value="Add" onclick="addRow('dataTable')"/></td>
    <td><input type="button" value="Delete" name="deleteBtn"  class="delete" onclick="deleteRow()"/></td>
  </tr>
</tr>
</table>
<table align="center">
  <tr>
    <td colspan="9">
      <input type="submit" value="SUBMIT" name="submitBtn" />
      <input type="submit" value="GRN Print" name="grnPrintBtn" />
      <input type="submit" value="INFO Print" name="infoPrintBtn" />
      <input type="submit" value="SEARCH" name="searchBtn" />
      <input type="submit" value="CANCEL" name="cancelBtn" />
    </td>
  </tr>
</table>
</form>
<?php $_template = new Smarty_Internal_Template("footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>