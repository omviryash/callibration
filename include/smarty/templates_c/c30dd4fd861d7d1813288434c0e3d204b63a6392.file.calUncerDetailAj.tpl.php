<?php /* Smarty version Smarty-3.0.6, created on 2015-01-11 16:47:57
         compiled from "./templates\calUncerDetailAj.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2068854b29b2d3716b6-23519448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c30dd4fd861d7d1813288434c0e3d204b63a6392' => 
    array (
      0 => './templates\\calUncerDetailAj.tpl',
      1 => 1420991271,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2068854b29b2d3716b6-23519448',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<center class="table1"><h2>Krishna Instrument</h2></center>
<center class="table1"><a href="calUncertainty.php"> ! Calculation Of Uncertainty ! </a></center><br>
<?php if ($_smarty_tpl->getVariable('recordNotFound')->value==''){?>
<table align='center' border='1' cellpadding='1' cellspacing='0'>
	<tr>
		<td class="table2" colspan="7">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b><?php echo $_smarty_tpl->getVariable('mainArray')->value['parameter'];?>
</b>
		</td>
	</tr>
	<tr>
		<td>GRN : </td>
		<td>N-<?php echo $_smarty_tpl->getVariable('mainArray')->value['grnId'];?>
</td>
		<td colspan="3"><b>Uncertainty calibration certificate</b></td>
		<td><?php echo $_smarty_tpl->getVariable('mainArray')->value['uncertaintyCalibration'][0];?>
</td>
		<td>%</td>
	</tr>
	<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['name'] = "trSec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['newTr']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["trSec"]['total']);
?>
	<?php if ($_smarty_tpl->getVariable('mainArray')->value['newTr'][$_smarty_tpl->getVariable('smarty')->value['section']['trSec']['index']]==1){?>
	<tr>
		<td colspan="2">&nbsp;</td>
	  <td colspan="3"><b>Uncertainty calibration certificate</b></td>
	  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['uncertaintyCalibration'][$_smarty_tpl->getVariable('smarty')->value['section']['trSec']['index']];?>
</td>
	  <td>%</td>
	</tr>
	<?php }?>
	<?php endfor; endif; ?>
	<tr>
		<td>Date : </td>
		<td><?php echo $_smarty_tpl->getVariable('mainArray')->value['callibrationDate'];?>
</td>
		<td colspan="3"><b>Degree of freedom</b></td>
		<td><?php echo $_smarty_tpl->getVariable('mainArray')->value['degreeOfFreedom'];?>
</td>
		<td>V1</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><b>Accuracy%</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['accuracyTaken']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['accuracyTaken'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="5"><b>Type A uncertainty</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['accuracyTaken']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><b><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['secA']['index_next'];?>
<b></td>
		<?php endfor; endif; ?>
 </tr>
	<tr>
		<td>Resolution</td>
		<td><?php echo $_smarty_tpl->getVariable('mainArray')->value['parameter'];?>
</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['accuracyTaken']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['resolution'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
 </tr>
	<tr>
		<td>1</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdMtr1']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdMtr1'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>2</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdMtr2']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdMtr2'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
		</tr>
	<tr>
		<td>3</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdMtr3']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdMtr3'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>4</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdMtr4']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdMtr4'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>5</td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdMtr5']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdMtr5'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td><b>Mean reading</b></td>
		<td><b>Avg.</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['meanReading']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['meanReading'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
  </tr>
	<tr>
		<td><b>Standard deviation</b></td>
		<td>&nbsp;</td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdDeviation']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdDeviation'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td><b>Standard Uncertainty</b></td>
		<td><b>Ur</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdUncertinity']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdUncertinity'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td><b>Standard Uncertainty</b></td>
		<td><b>%Ur</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['stdUncertinityPer']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['stdUncertinityPer'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="5"><b>Type B Uncertainty</b></td>
	</tr>
	<tr>
		<td><b>Defree of freedom</b></td>
		<td>&prop;</td>
		<td colspan="2"><b>Confidance leval</td>
		<td>95%</td>
		<td>Covarage factor</td>
		<td>1.96</td>
	</tr>
	<tr>
		<td>Uncertinty of master</td>
		<td><b>UB1</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['uncertOfMaster']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['uncertOfMaster'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>Accuracy of cali. Certi.</td>
		<td><b>UB2</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['accuOfCaliCerty']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['accuOfCaliCerty'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>Resolution</td>
		<td><b>UB3</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['resolutionTypeB']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['resolutionTypeB'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>Combined Uncertinty</td>
		<td><b>Uc</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['combinedUncert']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['combinedUncert'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>Expanded Uncertinty</td>
		<td><b>Ue</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['expanUncert']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['expanUncert'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><b>%Ue</b></td>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['name'] = "secA";
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('mainArray')->value['expanUncertInPer']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["secA"]['total']);
?>
		  <td><?php echo $_smarty_tpl->getVariable('mainArray')->value['expanUncertInPer'][$_smarty_tpl->getVariable('smarty')->value['section']['secA']['index']];?>
</td>
		<?php endfor; endif; ?>
	</tr>
</table>
<?php }else{ ?>
  <center><h1 style="color:red;"><?php echo $_smarty_tpl->getVariable('recordNotFound')->value;?>
</h1></center>
<?php }?>
<!--<table align='center' border='0' cellpadding='1' cellspacing='2'>
  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('grnObsArray')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td class="table1" align='center'>Std. Meter Value</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('grnObsArray')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter3')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter4')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeter5')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('stdMeterAverage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
  </tr>
  <tr>
    <td  class="table1" align='center'>Test Meter Value</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter1')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter2')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter3')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter4')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeter5')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
    <td class="table2" align="right"><?php echo $_smarty_tpl->getVariable('testMeterAverage')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']];?>
</td>
  </tr><tr></tr><tr></tr><tr></tr>
  
  <?php endfor; endif; ?>
</table>-->