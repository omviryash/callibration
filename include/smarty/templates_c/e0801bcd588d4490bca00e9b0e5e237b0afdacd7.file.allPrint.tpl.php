<?php /* Smarty version Smarty-3.0.6, created on 2015-02-02 19:01:45
         compiled from "./templates\allPrint.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2427854cfbb892a3e40-50304538%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e0801bcd588d4490bca00e9b0e5e237b0afdacd7' => 
    array (
      0 => './templates\\allPrint.tpl',
      1 => 1413482390,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2427854cfbb892a3e40-50304538',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("./headStartPrint.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("./headEndPrint.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>
<form name="PrintForm" id="printForm" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>
">
<table border="1" cellSpacing="0" cellpadding="2">
    <tr>
      <th align="right" colspan="2">
        <a href="grnList.php"><img src="./images/logo.jpg" border="0" width="300" height="130" /></a>
      </th>
    </tr>
    <tr>
      <td>
      <table border="0" cellSpacing="0" cellpadding="2">
      <tr>
        <td> GRN No. :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnPrefix')->value;?>
-<?php echo $_smarty_tpl->getVariable('grnNo')->value;?>
</td>
        <td></td>
        <td align="right"> Date :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnDate')->value;?>
</td>
        <td></td>
        <td> P.O /Letter No :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('poNo')->value;?>
</td>
        <td> Date :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('poDate')->value;?>
</td>
      </tr>
      <tr>
        <td colspan="2"> Customer / Supplier Name :</td>
        <td class="table2" align="left" colspan="2  "><?php echo $_smarty_tpl->getVariable('custName')->value;?>
</td>
        <td align="right">CODE :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('custCode')->value;?>
</td>
      </tr>
      <tr>
        <td> Contact Person :</td>
        <td class="table2" align="left" colspan="3"><?php echo $_smarty_tpl->getVariable('mrAndMrs')->value;?>
.  <?php echo $_smarty_tpl->getVariable('contPerson')->value;?>
</td>
        <td colspan="2"align="right"> PH. No. :</td>
        <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('phNo')->value;?>
</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td align="center" colspan="2">
    <table border="1" cellSpacing="0" cellpadding="2">
      <tr><th colspan="11">FOLLOWING MATERIALS RECEIVED FOR - CALIBRATION /VERIFICATION</th> </tr>
      <tr>
      <th rowspan="2">SR NO.</th>
      <th rowspan="2">NAME OF INSTRUMENT</th>
      <th rowspan="2">ITEM ID Code</th>
      <th rowspan="2">PARAMETERS</th>
      <th rowspan="2">Range</th>
      <th colspan="3" align="center">Q U A N T I T Y</th>
      <th rowspan="2">Cali Due Date Requested By Cust.</th>
      <th rowspan="2">Expected Delivery Date</th>
      <th rowspan="2">Notes</th>
      </tr>
      <tr>
      <th>CHALLAN</th>
      <th>RECEIVED</th>
      <th>CONDITION</th>
      </tr>
      <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->getVariable('grnDetail')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
       <tr>
         <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum'];?>
</td>
         <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['itemName'];?>
</td>
         <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['itemCode'];?>
</td>
         <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['parameterName'];?>
</td>
         <td class="table2" align="left"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['range'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['challan'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['received'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grnCondition'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['custReqDate'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['expDelivDate'];?>
</td>
         <td class="table2" align="center"><?php echo $_smarty_tpl->getVariable('grnDetail')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['description'];?>
</td>
         <tr class="table2" align="left">
       <?php endfor; else: ?>
       <?php endif; ?>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan="2"> Remarks : <?php echo $_smarty_tpl->getVariable('remarks')->value;?>
</td>
  </tr>
  <tr>
    <td> Received By : <?php echo $_smarty_tpl->getVariable('userName')->value;?>
</td>
    <td> Store HOD : </td>
  </tr>
  <tr><td> For delivery of materials this goods Receipt required,otherwise material will not be returned.</td></tr>
</table>

</form>
<?php $_template = new Smarty_Internal_Template("./footerPrint.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>