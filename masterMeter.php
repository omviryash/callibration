<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $masterMeterId             = "";
  $masterMeterName           = "";
  $masterMeterIdNo           = "";
  $masterMeterMake           = "";
  $masterMeterModelNo        = "";
  $masterMeterSerialNo       = "";
  $masterMeterCertificateNo  = "";
  $masterMeterTraceabilityTo = "";
  $masterMeterExpDate        = "";
  $parameter                 = "";
  $procedureText             = "";
  $range                     = "";
  $accuracySubSub            = 0;
  $uncertinity               = 0;
  $certiAccuracy             = 0;
  $resolution                = 0;
  $stability                 = 0;
  $masterUncertaintyText     = "";
  $isEdit                    = 0;
  $masterMeterExp            = "";
  $meterEntry                = array();
  $masterMeterId             = isset($_REQUEST['masterMeterId']) ? $_REQUEST['masterMeterId'] : 0;
  // Master Meter Name Entry : Starts
  if(isset($_REQUEST['insertBtn']))
  {
  	if(isset($_REQUEST['masterMeterId']) && $_REQUEST['masterMeterId'] > 0)
  	{
  		$masterMeterExp   = ($_POST['masterMeterExpDateYear']."-".$_POST['masterMeterExpDateMonth']."-".$_POST['masterMeterExpDateDay']);
  		$masterUpdate = "UPDATE mastermeter 
  		                    SET masterMeterName           = '".$_POST['masterMeterName']."',
  		                        masterMeterIdNo           = '".$_POST['masterMeterIdNo']."',
  		                        masterMeterMake           = '".$_POST['masterMeterMake']."',
  		                        masterMeterModelNo        = '".$_POST['masterMeterModelNo']."',
  		                        masterMeterSerialNo       = '".$_POST['masterMeterSerialNo']."',
  		                        masterMeterCertificateNo  = '".$_POST['masterMeterCertificateNo']."',
  		                        masterMeterExp            = '".$masterMeterExp."',
  		                        masterMeterTraceabilityTo = '".$_POST['masterMeterTraceabilityTo']."',
  		                        procedureText             = '".$_POST['procedureText']."',
  		                        masterUncertaintyText     = '".$_POST['masterUncertaintyText']."'
  		                  WHERE masterMeterId = ".$_POST['masterMeterId'];
  	  $masterUpdateRes = mysql_query($masterUpdate);
  	  
  	  //$deleteParamaeters = mysql_query("DELETE FROM mastermetersub WHERE masterMeterId = ".$_POST['masterMeterId']."");
  	  
  	  // Parameter of Master Meter Entry : Starts
      $loopCount = 0;
      while($loopCount < count($_POST['parameter']))
      {
        $parameter     = ($_POST['parameter'][$loopCount] != '') ? $_POST['parameter'][$loopCount] : 0;
       
        if($_POST['masterMeterSubId'][$loopCount] != '' && $_POST['masterMeterSubId'][$loopCount] > 0 )
        {
        	$subMMUpdate = "UPDATE mastermetersub
        	                   SET parameter = '".$parameter."'
        	                 WHERE masterMeterSubId = ".$_POST['masterMeterSubId'][$loopCount];
        	$subMMUpdateRes = mysql_query($subMMUpdate);
        }
        else
        {
          if($_POST['parameter'][$loopCount] != "")
          {
            $insertMeterPara       = "INSERT INTO mastermetersub(masterMeterId,parameter)
                                      VALUES(".$_POST['masterMeterId'].",'".$parameter."')";
            $insertMeterParaResult = mysql_query($insertMeterPara);
            $masterMeterSubIdInserted = mysql_insert_id();
            
          }
        }
	    	  $loopCount++;
      }
      // Parameter of Master Meter Entry : Ends
  	  $masterMeterId = 0;
    }
    else
    {
      $masterMeterExp        = ($_POST['masterMeterExpDateYear']."-".$_POST['masterMeterExpDateMonth']."-".$_POST['masterMeterExpDateDay']);
      $procedureText         = ($_POST['procedureText'] != '') ? $_POST['procedureText'] : 0;
      $masterUncertaintyText = ($_POST['masterUncertaintyText'] != '') ? $_POST['masterUncertaintyText'] : 0;
      $selectmasterMeterName    = "INSERT INTO mastermeter(masterMeterId,masterMeterName,masterMeterIdNo,masterMeterMake,masterMeterModelNo,masterMeterSerialNo,
                                                           masterMeterCertificateNo,masterMeterExp,masterMeterTraceabilityTo,procedureText,masterUncertaintyText)
                                   VALUES('".$masterMeterId."','".$_POST['masterMeterName']."','".$_POST['masterMeterIdNo']."','".$_POST['masterMeterMake']."',
                                                               '".$_POST['masterMeterModelNo']."','".$_POST['masterMeterSerialNo']."','".$_POST['masterMeterCertificateNo']."',
                                                               '".$masterMeterExp."','".$_POST['masterMeterTraceabilityTo']."','".$procedureText."','".$masterUncertaintyText."')";
      $selectmasterMeterNameRes = mysql_query($selectmasterMeterName);
      $joinId		                = mysql_insert_id();
      
      if(!$selectmasterMeterNameRes)
        die("Insert Query Not Inserted : ".mysql_error());
      // Master Meter Name Entry : Ends
      // Parameter of Master Meter Entry : Starts
      $loopCount = 0;
      while($loopCount < count($_POST['parameter']))
      {
        $parameter     = ($_POST['parameter'][$loopCount] != '') ? $_POST['parameter'][$loopCount] : 0;
       
        if($_POST['parameter'][$loopCount] != "")
        {
          $insertMeterPara       = "INSERT INTO mastermetersub(masterMeterId,parameter)
                                    VALUES(".$joinId.",'".$parameter."')";
          $insertMeterParaResult = mysql_query($insertMeterPara);
          $masterMeterSubIdInserted = mysql_insert_id();
          
        }
		   	  $loopCount++;
      }
      // Parameter of Master Meter Entry : Ends
       header("Location:masterMeter.php");
    }
  }
  
  // Master Meter Name Listing : Starts
  $meterNameList  = "SELECT masterMeterId,masterMeterName,masterMeterIdNo,masterMeterMake,masterMeterModelNo,masterMeterSerialNo,masterMeterCertificateNo,
                            masterMeterExp,masterMeterTraceabilityTo,procedureText,masterUncertaintyText
                       FROM mastermeter
                      ORDER BY masterMeterName";
  $meterNameListResult    = mysql_query($meterNameList);
  $meterListRow           = mysql_num_rows($meterNameListResult);
  $i = 0;
  $parameterArr = array();
  while($meterListRow = mysql_fetch_array($meterNameListResult))
  {
    $meterEntry[$i]['masterMeterId']             = $meterListRow['masterMeterId'];
    $meterEntry[$i]['masterMeterName']           = $meterListRow['masterMeterName'];
    $meterEntry[$i]['masterMeterIdNo']           = $meterListRow['masterMeterIdNo'];
    $meterEntry[$i]['masterMeterMake']           = $meterListRow['masterMeterMake'];
    $meterEntry[$i]['masterMeterModelNo']        = $meterListRow['masterMeterModelNo'];
    $meterEntry[$i]['masterMeterSerialNo']       = $meterListRow['masterMeterSerialNo'];
    $meterEntry[$i]['masterMeterCertificateNo']  = $meterListRow['masterMeterCertificateNo'];
    $meterEntry[$i]['masterMeterExp']            = $meterListRow['masterMeterExp'];
    $meterEntry[$i]['masterMeterTraceabilityTo'] = $meterListRow['masterMeterTraceabilityTo'];
    $meterEntry[$i]['procedureText']             = $meterListRow['procedureText'];
    $meterEntry[$i]['masterUncertaintyText']     = $meterListRow['masterUncertaintyText'];
    
     if($meterListRow['masterMeterId'] == $masterMeterId)
     {
     	 $isEdit = 1;
       $masterMeterName           = $meterListRow['masterMeterName'];
       $masterMeterIdNo           = $meterListRow['masterMeterIdNo'];
       $masterMeterMake           = $meterListRow['masterMeterMake'];
       $masterMeterModelNo        = $meterListRow['masterMeterModelNo'];
       $masterMeterSerialNo       = $meterListRow['masterMeterSerialNo'];
       $masterMeterCertificateNo  = $meterListRow['masterMeterCertificateNo'];
       $masterMeterExp            = $meterListRow['masterMeterExp'];
       $masterMeterTraceabilityTo = $meterListRow['masterMeterTraceabilityTo'];
       $masterUncertaintyText     = $meterListRow['masterUncertaintyText'];
       $selectParameters = "SELECT masterMeterSubId,parameter,masterMeterId FROM mastermetersub WHERE masterMeterId =".$meterListRow['masterMeterId'];
       $parameterRes = mysql_query($selectParameters);
       $count = 0;
       while($parameterRow = mysql_fetch_array($parameterRes))
       {
       	 $parameterArr[$count]['masterMeterSubId'] = $parameterRow['masterMeterSubId'];
       	 $parameterArr[$count]['parameter'] = $parameterRow['parameter'];
       	 $count++;
       }
       $procedureText             = $meterListRow['procedureText'];
     }  
     $i++;
  }

  include("./bottom.php");
  $smarty->assign("masterMeterExpDate",$masterMeterExpDate);
  $smarty->assign("parameter",$parameter);
  $smarty->assign("masterMeterName",$masterMeterName);
  $smarty->assign("masterMeterIdNo",$masterMeterIdNo);
  $smarty->assign("masterMeterMake",$masterMeterMake);
  $smarty->assign("masterMeterModelNo",$masterMeterModelNo);
  $smarty->assign("masterMeterSerialNo",$masterMeterSerialNo);
  $smarty->assign("masterMeterCertificateNo",$masterMeterCertificateNo);
  $smarty->assign("masterMeterExp",$masterMeterExp);
  $smarty->assign("masterMeterTraceabilityTo",$masterMeterTraceabilityTo);
  $smarty->assign("procedureText",$procedureText);
  $smarty->assign("masterUncertaintyText",$masterUncertaintyText);
  $smarty->assign("range",$range);
  $smarty->assign("accuracySubSub",$accuracySubSub);
  $smarty->assign("resolution",$resolution);
  $smarty->assign("stability",$stability);
  $smarty->assign("uncertinity",$uncertinity);
  $smarty->assign("certiAccuracy",$certiAccuracy);
  $smarty->assign("masterMeterId",$masterMeterId);
  $smarty->assign("meterEntry",$meterEntry);
  $smarty->assign("isEdit",$isEdit);
  $smarty->assign("parameterArr",$parameterArr);
  $smarty->display("masterMeter.tpl");
}
?>