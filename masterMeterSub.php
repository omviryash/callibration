<?php
include("include/omConfig.php");

if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
	$isEdit              = 0;
	$editArray           = 0;
	//$editArray['masterMeterSubId'] = 0;
  $masterMeterId       = "";
  $masterMeterName     = "";
  $masterMeterExpDate  = "";
  $parameter           = "";
  $range               = "";
  $accuracy            = 0;
  $accuracySubSub      = 0;
  $accuracySubSubFS    = 0;
  $uncertinity         = 0;
  $certiAccuracy       = 0;
  $degreeOfFreedom     = 0;
  $accuracy            = 0;
  $resolution          = 0;
  $stability           = 0;
  $masterMeterExp      = "";
  $meterEntry          = array();
  $meterEntryNameArray = array();
  $meterEntryIdArray   = array();
  $meterEntryExpArray  = isset($_REQUEST['masterMeterId']) ? $_REQUEST['masterMeterId'] : 0;
  // Master Meter Name Entry : Starts

  if(isset($_REQUEST['insertBtn']))
  { 
      $masterMeterSubId = $_REQUEST['masterMeterSubId'];
      $loopCountSubSub = 0;
		  while($loopCountSubSub < count($_POST['range']))
		  {
		    $range            =  ($_POST['range'][$loopCountSubSub] != '') ? $_POST['range'][$loopCountSubSub] : 0;
		    $accuracySubSub   =  ($_POST['accuracySubSub'][$loopCountSubSub] != '') ? $_POST['accuracySubSub'][$loopCountSubSub] : 0;
		    $accuracySubSubFS =  ($_POST['accuracySubSubFS'][$loopCountSubSub] != '') ? $_POST['accuracySubSubFS'][$loopCountSubSub] : 0;
		    $resolution       =  ($_POST['resolution'][$loopCountSubSub] != '') ? $_POST['resolution'][$loopCountSubSub] : 0;
		    $stability        =  ($_POST['stability'][$loopCountSubSub] != '') ? $_POST['stability'][$loopCountSubSub] : 0;
		    $uncertinty       =  ($_POST['uncertinty'][$loopCountSubSub] != '') ? $_POST['uncertinty'][$loopCountSubSub] : 0;
		    $certiAccuracy    =  0;
		    $degreeOfFreedom  =  ($_POST['degreeOfFreedom'][$loopCountSubSub] != '') ? $_POST['degreeOfFreedom'][$loopCountSubSub] : 0;
		    
		    if(isset($_POST['masterMeterSubSubId']) && $_POST['masterMeterSubSubId'] > 0)
		    {
		    	$updateSubSub = "UPDATE mastermetersubsub
		    	                    SET rangeValue            = '".$range."',
		    	                        accuracySubSub   = '".$accuracySubSub."',
  		                            accuracySubSubFS = '".$accuracySubSubFS."',
  		                            resolution       = '".$resolution."',
  		                            stability        = '".$stability."',
  		                            uncertinty       = '".$uncertinty."',
  		                            certiAccuracy    = '".$certiAccuracy."',
  		                            degreeOfFreedom  = '".$degreeOfFreedom."'
  		                      WHERE masterMeterSubSubId = " .$_POST['masterMeterSubSubId'];
  		    $updateSubSubRes = mysql_query($updateSubSub);
  		    if(!$updateSubSubRes)
  		    {
  		    	die("Insert Query Not Inserted : ".mysql_error());
          }
		    }
		    else
		    {
	  	    if($_POST['range'][$loopCountSubSub] != "" || $_POST['accuracySubSub'][$loopCountSubSub] != "" || $_POST['accuracySubSubFS'][$loopCountSubSub] != "" || $_POST['resolution'][$loopCountSubSub] != "" || $_POST['stability'][$loopCountSubSub] != ""|| $_POST['uncertinty'][$loopCountSubSub] != "" || $_POST['certiAccuracy'][$loopCountSubSub] != "" || $_POST['degreeOfFreedom'][$loopCountSubSub] != "" )
	  	    {
	  	      $insertMeterSubPara       = "INSERT INTO mastermetersubsub(masterMeterSubId,rangeValue,accuracySubSub,accuracySubSubFS,resolution,stability,uncertinty,certiAccuracy,degreeOfFreedom)
	  	                                    VALUES(".$masterMeterSubId.",'".$range."','".$accuracySubSub."','".$accuracySubSubFS."','".$resolution."','".$stability."','".$uncertinty."','".$certiAccuracy."','".$degreeOfFreedom."')";
	  	      $insertMeterSubParaResult = mysql_query($insertMeterSubPara);
	  	    }
	  	  }
        $loopCountSubSub++;
      }
    //header("Location:masterMeterSub.php");
  }
  
  if(isset($_GET['masterMeterSubSubId']) && $_GET['masterMeterSubSubId'] > 0)
  {
  	$isEdit = 1;
  	
  	$editArray = array();
  	$selEditData = "SELECT mastermetersubsub.masterMeterSubSubId,	mastermetersubsub.masterMeterSubId,mastermetersub.masterMeterId,	
  	                       rangeValue,	accuracySubSub,	accuracySubSubFS,	resolution, stability,	uncertinty,	certiAccuracy,
  	                       degreeOfFreedom
  	                  FROM mastermetersubsub
  	                  JOIN mastermetersub ON mastermetersubsub.masterMeterSubId = mastermetersub.masterMeterSubId
  	                 WHERE masterMeterSubSubId = ".$_GET['masterMeterSubSubId'];
    $selEditDataRes = mysql_query($selEditData);
    if($editRow = mysql_fetch_array($selEditDataRes))
    {
    	$editArray['masterMeterSubSubId'] = $editRow['masterMeterSubSubId'];
    	$editArray['masterMeterSubId']    = $editRow['masterMeterSubId'];
    	$editArray['masterMeterId']       = $editRow['masterMeterId'];
    	$editArray['range']               = $editRow['rangeValue'];
    	$editArray['accuracySubSub']      = $editRow['accuracySubSub'];
    	$editArray['accuracySubSubFS']    = $editRow['accuracySubSubFS'];
    	$editArray['resolution']          = $editRow['resolution'];
    	$editArray['stability']           = $editRow['stability'];
    	$editArray['uncertinty']          = $editRow['uncertinty'];
    	$editArray['certiAccuracy']       = $editRow['certiAccuracy'];
    	$editArray['degreeOfFreedom']     = $editRow['degreeOfFreedom'];
    }
  }
  
  // Master Meter Name Listing : Starts
  $meterNameList          = "SELECT masterMeterId,masterMeterName,masterMeterExp
                               FROM mastermeter
                              ORDER BY masterMeterName";
  $meterNameListResult    = mysql_query($meterNameList);
  $meterListRow           = mysql_num_rows($meterNameListResult);
  $i = 0;
  while($meterListRow = mysql_fetch_array($meterNameListResult))
  {
    $meterEntryIdArray[$i]       = $meterListRow['masterMeterId'];
    $meterEntryNameArray[$i]     = $meterListRow['masterMeterName'];
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign("masterMeterExpDate",$masterMeterExpDate);
  $smarty->assign("parameter",$parameter);
  $smarty->assign("accuracy",$accuracy);
  $smarty->assign("range",$range);
  $smarty->assign("accuracySubSub",$accuracySubSub);
  $smarty->assign("accuracySubSubFS",$accuracySubSubFS);
  $smarty->assign("resolution",$resolution);
  $smarty->assign("stability",$stability);
  $smarty->assign("uncertinity",$uncertinity);
  $smarty->assign("certiAccuracy",$certiAccuracy);
  $smarty->assign("degreeOfFreedom",$degreeOfFreedom);
  $smarty->assign("masterMeterId",$masterMeterId);
  $smarty->assign("meterEntry",$meterEntry);
  $smarty->assign("meterEntryIdArray",$meterEntryIdArray);
  $smarty->assign("meterEntryNameArray",$meterEntryNameArray);
  $smarty->assign("meterEntryExpArray",$meterEntryExpArray);
  $smarty->assign("isEdit",$isEdit);
  $smarty->assign("editArray",$editArray);
  $smarty->display("masterMeterSub.tpl");
}
?> 