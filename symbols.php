<?php
include("include/omConfig.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $symbolArr = array();
  $selSymbol = "SELECT symbol,htmlCode
                          FROM symbols";
  $selSymbolResult = mysql_Query($selSymbol);
  $i = 0;
  while($symbolRow = mysql_fetch_array($selSymbolResult))
  {
    $symbolArr[$i]['symbol']   = $symbolRow['symbol'];
    $symbolArr[$i]['htmlCode'] = htmlentities( $symbolRow['htmlCode'] );
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign("symbolArr",$symbolArr);
  $smarty->display("symbols.tpl");
}
?>