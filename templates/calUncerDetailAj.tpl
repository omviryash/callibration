<center class="table1"><h2>Krishna Instrument</h2></center>
<center class="table1"><a href="calUncertainty.php"> ! Calculation Of Uncertainty ! </a></center><br>
{if $recordNotFound == ''}
<table align='center' border='1' cellpadding='1' cellspacing='0'>
	<tr>
		<td class="table2" colspan="7">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>{$mainArray.parameter}</b>
		</td>
	</tr>
	<tr>
		<td>GRN : </td>
		<td>N-{$mainArray.grnId}</td>
		<td colspan="3"><b>Uncertainty calibration certificate</b></td>
		<td>{$mainArray.uncertaintyCalibration[0]}</td>
		<td>%</td>
	</tr>
	{section name="trSec" loop=$mainArray.newTr}
	{if $mainArray.newTr[trSec] == 1}
	<tr>
		<td colspan="2">&nbsp;</td>
	  <td colspan="3"><b>Uncertainty calibration certificate</b></td>
	  <td>{$mainArray.uncertaintyCalibration[trSec]}</td>
	  <td>%</td>
	</tr>
	{/if}
	{/section}
	<tr>
		<td>Date : </td>
		<td>{$mainArray.callibrationDate}</td>
		<td colspan="3"><b>Degree of freedom</b></td>
		<td>{$mainArray.degreeOfFreedom}</td>
		<td>V1</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><b>Accuracy%</b></td>
		{section name="secA" loop=$mainArray.accuracyTaken}
		  <td>{$mainArray.accuracyTaken[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="5"><b>Type A uncertainty</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.accuracyTaken}
		  <td><b>{$smarty.section.secA.index_next}<b></td>
		{/section}
 </tr>
	<tr>
		<td>Resolution</td>
		<td>{$mainArray.parameter}</td>
		{section name="secA" loop=$mainArray.accuracyTaken}
		  <td>{$mainArray.resolution[secA]}</td>
		{/section}
 </tr>
	<tr>
		<td>1</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdMtr1}
		  <td>{$mainArray.stdMtr1[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>2</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdMtr2}
		  <td>{$mainArray.stdMtr2[secA]}</td>
		{/section}
		</tr>
	<tr>
		<td>3</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdMtr3}
		  <td>{$mainArray.stdMtr3[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>4</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdMtr4}
		  <td>{$mainArray.stdMtr4[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>5</td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdMtr5}
		  <td>{$mainArray.stdMtr5[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td><b>Mean reading</b></td>
		<td><b>Avg.</b></td>
		{section name="secA" loop=$mainArray.meanReading}
		  <td>{$mainArray.meanReading[secA]}</td>
		{/section}
  </tr>
	<tr>
		<td><b>Standard deviation</b></td>
		<td>&nbsp;</td>
		{section name="secA" loop=$mainArray.stdDeviation}
		  <td>{$mainArray.stdDeviation[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td><b>Standard Uncertainty</b></td>
		<td><b>Ur</b></td>
		{section name="secA" loop=$mainArray.stdUncertinity}
		  <td>{$mainArray.stdUncertinity[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td><b>Standard Uncertainty</b></td>
		<td><b>%Ur</b></td>
		{section name="secA" loop=$mainArray.stdUncertinityPer}
		  <td>{$mainArray.stdUncertinityPer[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td colspan="5"><b>Type B Uncertainty</b></td>
	</tr>
	<tr>
		<td><b>Defree of freedom</b></td>
		<td>&prop;</td>
		<td colspan="2"><b>Confidance leval</td>
		<td>95%</td>
		<td>Covarage factor</td>
		<td>1.96</td>
	</tr>
	<tr>
		<td>Uncertinty of master</td>
		<td><b>UB1</b></td>
		{section name="secA" loop=$mainArray.uncertOfMaster}
		  <td>{$mainArray.uncertOfMaster[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>Accuracy of cali. Certi.</td>
		<td><b>UB2</b></td>
		{section name="secA" loop=$mainArray.accuOfCaliCerty}
		  <td>{$mainArray.accuOfCaliCerty[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>Resolution</td>
		<td><b>UB3</b></td>
		{section name="secA" loop=$mainArray.resolutionTypeB}
		  <td>{$mainArray.resolutionTypeB[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>Combined Uncertinty</td>
		<td><b>Uc</b></td>
		{section name="secA" loop=$mainArray.combinedUncert}
		  <td>{$mainArray.combinedUncert[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>Expanded Uncertinty</td>
		<td><b>Ue</b></td>
		{section name="secA" loop=$mainArray.expanUncert}
		  <td>{$mainArray.expanUncert[secA]}</td>
		{/section}
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><b>%Ue</b></td>
		{section name="secA" loop=$mainArray.expanUncertInPer}
		  <td>{$mainArray.expanUncertInPer[secA]}</td>
		{/section}
	</tr>
</table>
{else}
  <center><h1 style="color:red;">{$recordNotFound}</h1></center>
{/if}
<!--<table align='center' border='0' cellpadding='1' cellspacing='2'>
  {section name="sec" loop=$grnObsArray}
  <tr>
    <td class="table1" align='center'>Std. Meter Value</td>
    <td class="table2" align="right">{$grnObsArray[sec]}</td>
    <td class="table2" align="right">{$stdMeter2[sec]}</td>
    <td class="table2" align="right">{$stdMeter3[sec]}</td>
    <td class="table2" align="right">{$stdMeter4[sec]}</td>
    <td class="table2" align="right">{$stdMeter5[sec]}</td>
    <td class="table2" align="right">{$stdMeterAverage[sec]}</td>
  </tr>
  <tr>
    <td  class="table1" align='center'>Test Meter Value</td>
    <td class="table2" align="right">{$testMeter1[sec]}</td>
    <td class="table2" align="right">{$testMeter2[sec]}</td>
    <td class="table2" align="right">{$testMeter3[sec]}</td>
    <td class="table2" align="right">{$testMeter4[sec]}</td>
    <td class="table2" align="right">{$testMeter5[sec]}</td>
    <td class="table2" align="right">{$testMeterAverage[sec]}</td>
  </tr><tr></tr><tr></tr><tr></tr>
  
  {/section}
</table>-->