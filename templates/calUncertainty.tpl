{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function() {
  $('#grnId').focus();
  $("#grnId").change(function(){
    var datastring = "grnId=" + $('#grnId').val();
    $.ajax({
      url: "./observationGrnAj.php",
      data: datastring,
      success: function(data){
        $('#instrument').html(data);
        $("#grnDetailId").change(function(){
          var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val();
          $.ajax({
            url: "./calUncerDetailAj.php",
            data: datastring,
            success: function(response){
              $('#obsDetail').html(response);
              $('#grnIdAj').val($('#grnId').val());
              $('#grnDetailIdAj').val($('#grnDetailId').val());
              $('input[name=buttonPrint]').hide();
              $('#printBtn').append('<input type="submit" name="buttonPrint" value=" P R I N T " />');
            }
          });
        });
      }
    });
  });
});
</script>
{include file="./headEnd.tpl"}
<form name="printData" id="printData" method="get" action="calUncerDetailAj.php">
<input type="hidden" name="grnId" id="grnIdAj" />
<input type="hidden" name="grnDetailId" id="grnDetailIdAj" />
<span id="printBtn"></span>
</form>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Customer Id</td>
</tr>
<tr>
  <td>
    <select name="grnId" id="grnId">
      <option value="0">GRN List</option>
      {html_options values=$grnId output=$grnPrefixNo}
    </select>
  </td>
  <td>
    <span id="instrument">
      <select name="grnDetailId" id="grnDetailId">
        <option>Select Instrument</option>
      </select>
    </span>
  </td>
</tr>
</table>
<br/>
<div id="obsDetail"></div>