{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function(){
  $(".link").click(function(){
    $("#grnList").attr("action","grnItemListPrint.php");
    $("#grnList").submit();
    
  });
});
</script>
{include file="./headEnd.tpl"}
<form id="grnList" action="{$smarty.server.PHP_SELF}" name='grnList' method='POST'>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<center><h2>G.R.N. Item List</h2></center><br>
<tr>
<td class="table1" align="center" colspan="6">
	Select From : {html_select_date prefix="fromDate" start_year="-5" end_year="+2" field_order="DMY" time=$fromDate day_value_format="%02d"}
	To : {html_select_date prefix="toDate" start_year="-2" end_year="+1" field_order="DMY" time=$toDate day_value_format="%02d"}
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	Status : 
	<select name="dispatch">
		{if $dispatchValue == 0}
			<option value="0" SELECTED >All</option>
			<option value="1">Dispatched</option>
			<option value="2">Pending</option>
		{else if $dispatchValue == 1}
			<option value="0">All</option>
			<option value="1" SELECTED >Dispatched</option>
			<option value="2">Pending</option>
		{else if $dispatchValue == 2}
			<option value="0">All</option>
			<option value="1">Dispatched</option>
			<option value="2" SELECTED>Pending</option>
		{/if}
	</select>
<input type="submit" name="submitDateBtn" value="GO..." class="button"/>
</td>
</tr>
<tr>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Date</td>
  <td class="table1" align="center">Sr.no</td>
  <td class="table1" align="center">Instument Name</td>
  <td class="table1" align="center">Due Date</td>
  <td class="table1" align="center">Dispatched</td>
</tr>
 {assign var="srNo" value="1"}
 {section name="sec" loop=$grnCount}
<tr>
  {if $grnArray[sec].display == 1}
    {assign var="srNo" value="1"}
	  <td class="table2" align="center">{$grnArray[sec].grnPrefix}-{$grnArray[sec].grnNo}</td>
	  <td class="table2" align="center" NOWRAP>{$grnArray[sec].grnDate}</td>
	{else}
	  <td class="table3">&nbsp;</td>
	  <td class="table3">&nbsp;</td>	
	{/if}  
  <td class="table2" align="center">{$srNo++}</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].itemId}</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].grnDueDate}</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].dispatch}</td>
</tr>
{sectionelse}
<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>
{/section}
<tr><td align="center" colspan="22"> <h4><a href="#" class="link"><font color="red"><b>Print</b></a></h4></font></td></tr>
</table>
</form>
{include file="footer.tpl"}