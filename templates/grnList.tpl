{include file="./headStart.tpl"}
<script type="text/javascript">
function deletechecked()
{
  var answer = confirm("Delete Selected Recored ?")
  if (answer)
    document.messages.submit();
  return false;
}
</script>
<script type="text/javascript">
{literal}
function comboedit(comboValue,grnId,grnNo)
{
  if(comboValue.value == 'Edit')
    location.href ="grnEdit.php?grnId="+grnId;
  if(comboValue.value == 2)
  {
    if(confirm("Are You Sure ?"))
      location.href ="grnListDelete.php?grnId="+grnId;
  }
  if(comboValue.value == 3)
    location.href ="grnPrint.php?display=grnDetail&grnId="+grnId;
  if(comboValue.value == 4)
    location.href ="grnPrint.php?display=info&grnId="+grnId;
  if(comboValue.value == 5)
    location.href ="grnPrint.php?display=all&grnId="+grnId;
  if(comboValue.value == 6)
    location.href ="grnPrintpdf.php?grnId="+grnId;
  if(comboValue.value == 7)
    location.href ="grnPrintpdfInfo.php?grnId="+grnId;
  if(comboValue.value == 8)
    location.href ="grnDispatch.php?grnId="+grnId;
  // additional change by girish - start
  if(comboValue.value == 'copygrn')
  	if(confirm("Are you sure to copy "+grnNo+"?"))
	  	location.href ="grnCopy.php?nabl=0&grnId="+grnId;
	else 
		comboValue.selectedIndex = 0;	
  if(comboValue.value == 'copygrnnabl')
  	if(confirm("Are you sure to copy "+grnNo+" along with NABL?"))
	  	location.href ="grnCopy.php?nabl=1&grnId="+grnId;
	else 
		comboValue.selectedIndex = 0;	
  if(comboValue.value == 55)
    location.href ="grnPrint_newaddr.php?display=all&grnId="+grnId;		
  if(comboValue.value == 66)
    location.href ="grnPrintpdf_newaddr.php?grnId="+grnId;
  if(comboValue.value == 77)
    location.href ="grnPrintpdfInfo_newaddr.php?grnId="+grnId;	
  // additional change by girish - end  		
}
{/literal}
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" name='grnList' method='POST'>
<BODY onLoad="noBack();" onpageshow="if (event.persisted) noBack();" onUnload="">
<center class="center"><h2>G.R.N.  List</h2></center><br>
<table border='0' cellpadding='1' cellspacing='1' align='center'>
<tr>
<td class="table1" align="center" colspan="9">
	Select From : {html_select_date prefix="fromDate" start_year="-5" end_year="+2" field_order="DMY" time=$fromDate day_value_format="%02d"}
	To : {html_select_date prefix="toDate" start_year="-2" end_year="+1" field_order="DMY" time=$toDate day_value_format="%02d"}
<input type="submit" name="submitDateBtn" value="GO..." class="button"/>
</td>
</tr>
<tr>
  <td></td>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Date</td>
  <td class="table1" align='center'><B>PO / Letter No</B></td>
  <td class="table1" align='center'><B>PO Date</B></td>
  <td class="table1" align='center'><B>Customer Name</B></td>
  <td class="table1" align='center'><B>Customer Code</B></td>
  <td class="table1" align='center'><B>Contact Preson</B></td>
  <td class="table1" align='center'><B>Ph No</B></td>
</tr>

 {section name="sec" loop=$grnCount}
<tr>
	 <td class="table2" align="center">
	 	<!-- additional change by girish - start -->
		<!--select name="customerId" id="customerId" onChange="comboedit(this, {$grnArray[sec].grnId});"-->
		<select name="customerId" id="customerId" onChange="comboedit(this, {$grnArray[sec].grnId}, '{$grnArray[sec].grnPrefix}-{$grnArray[sec].grnNo}');">
		<!-- additional change by girish - end -->
      <option value="0">Select Action</option>
      <option value="Edit">Edit</option>
      <option value="2">Delete</option>
      <option value="5">All (Old Address)</option>
	  <option value="55">All (New Address)</option>
      <option value="6">Print GRN (Old Address)</option>
	  <option value="66">Print GRN (New Address)</option>
      <option value="7">Print Info Sheet (Old Address)</option>
	  <option value="77">Print Info Sheet (New Address)</option>
      <option value="8">Dispatch</option>
	  <!-- additional change by girish -- start -->
	  <option value="copygrn">Copy (Only GRN)</option>
	  <option value="copygrnnabl">Copy (GRN with NABL)</option>
	  <!-- additional change by girish -- end -->
    </select>
	</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].grnPrefix}-{$grnArray[sec].grnNo}</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].grnDate}</td>
	<td class="table2" align="center">{$grnArray[sec].poNo}</td>
	<td class="table2" align="center" NOWRAP>{$grnArray[sec].poDate}</td>
	<td class="table2" align="left" NOWRAP>{$grnArray[sec].custName}</td>
	<td class="table2" align="center">{$grnArray[sec].custCode}</td>
	<td class="table2" align="center">{$grnArray[sec].contPerson}</td>
	<td class="table2" align="center">{$grnArray[sec].phNo}</td>
</tr>
{sectionelse}
<tr><td align="center" colspan="22"> <h1><font color="red"><b>Record Not Found...!</b></h1></font></td></tr>
{/section}
</table>
</form>
{include file="footer.tpl"}