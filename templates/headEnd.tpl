{if strlen($s_activId) > 0}
<script type="text/javascript">
//SuckerTree Horizontal Menu (Sept 14th, 06)
//By Dynamic Drive: http://www.dynamicdrive.com/style/
var menuids=["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas

function buildsubmenus_horizontal()
{
  for (var i=0; i<menuids.length; i++)
  {
    var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++)
    {
  		if(ultags[t].parentNode.parentNode.id==menuids[i])
  		{ //if this is a first level submenu
  			ultags[t].style.top=ultags[t].parentNode.offsetHeight+"px" //dynamically position first level submenus to be height of main menu item
  			ultags[t].parentNode.getElementsByTagName("a")[0].className="mainfoldericon"
		  }
		  else
		  { //else if this is a sub level menu (ul)
  		  ultags[t].style.left=ultags[t-1].getElementsByTagName("a")[0].offsetWidth+"px" //position menu to the right of menu item that activated it
      	ultags[t].parentNode.getElementsByTagName("a")[0].className="subfoldericon"
		  }
      ultags[t].parentNode.onmouseover=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="visible"
      }
      ultags[t].parentNode.onmouseout=function()
      {
        this.getElementsByTagName("ul")[0].style.visibility="hidden"
      }
    }
  }
}

if (window.addEventListener)
window.addEventListener("load", buildsubmenus_horizontal, false)
else if (window.attachEvent)
window.attachEvent("onload", buildsubmenus_horizontal)

</script>
{/if}
</head>
<body>

<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td class="content">
    {if strlen($s_activId) > 0}
      <a href="logout.php" class="link">Logout</a> |
      <b>User Name : {$s_activId}</b>
      <span style="float:right"><a href="backup.php" class="link">Backup</a> </span>
    {else}
      <a href="checkLogin.php" class="link">Login</a>
    {/if}
    <br /><br />
    </td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        {if strlen($s_activId) > 0}
        <tr>
          <td class="bluebg1">
            <div class="suckertreemenu">
            <br />
            <ul id="treemenu1">
              <li><a href="#">Master</a>
                <ul>
                  <li><a href="grnPrefix.php" class="link">GRN Prefix</a></li>
                  <li><a href="item.php" class="link">Item</a></li>
                  <li><a href="parameterEntry.php" class="link">Parameter</a></li>
                  <li><a href="masterMeter.php" class="link">Master Meter</a></li>
                  <li><a href="masterMeterSub.php" class="link">Master Meter Sub</a></li>
                  <li><a href="#" class="link">Customer</a>
                    <ul style="padding-left: 0px;">
                      <li><a href="custEntry.php" class="link">Entry</a></li>
                      <li><a href="custList.php" class="link">List</a></li>
                    </ul>
                  </li>
                  <li><a href="symbols.php" class="link" target="_blank">SYMBOLS</a></li>
                </ul>
              </li>
              <li><a href="#" class="link">G.R.N.</a>
                <ul>
                  <li><a href="grnEntry.php" class="link">Entry</a></li>
                  <li><a href="grnList.php" class="link">List</a></li>
                  <li><a href="grnItemList.php" class="link">Item List</a></li>
                </ul>
              </li>
              {if strlen($s_userType) > 0 && $s_userType == "Admin"}
                <li><a href="staffEntry.php" class="link">Staff Entry</a></li>
              {/if}
              <li><a href="#" class="link">Observation Sheet</a>
                <ul>
                  <li><a href="observationSheet.php" class="link">NABL ENTRY</a></li>
                  <!--li><a href="observationSheetRegular.php" class="link">REGULAR ENTRY</a></li-->
                  <li><a href="observationList.php" class="link">List</a></li>
                  <li><a href="observationPrint.php" class="link">Print</a></li>
                  <li><a href="calUncertainty.php" class="link">Calculation of Uncertainty</a></li>
                  <li><a href="listOfCertificate.php" class="link">List of Certificate</a></li>
				  <!-- additional change by girish - start -->
				  <li><a href="instrumentsDues.php" class="link">Instruments Dues</a></li>
				  <!-- additional change by girish - end -->
                </ul>
              </li>
              <li><a href="deliveryChallan.php" class="link">Delivery Challan</a></li>
            </ul>
            <br /><br /><br />
            </div>
          </td>
        </tr>
        {/if}
        <tr>
          <td class="content">
<br /><br />
	