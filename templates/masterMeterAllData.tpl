<table border="2" width="100%" id="dataTable" align="center" left="10"cellpadding="0" cellspacing="0">
<tr>
  <td align="center" class="table1">&nbsp;</td>
  <td align="center" class="table1">Parameter</td>
  <td align="center" class="table1">Range</td>
  <td align="center" class="table1">Accuracy % of Rdg</td>
  <td align="center" class="table1">Accuracy % of FS</td>
  <td align="center" class="table1">Resolution</td>
  <td align="center" class="table1">Stability</td>
  <td align="center" class="table1">Uncertinity</td>
  <td align="center" class="table1">Certi. Accuracy</td>
  <td align="center" class="table1">&nbsp;</td>
</tr>
{section name=sec loop=$meterSubSubData}
<tr>
  <td align="center" class="table2">
    <a href="masterMeterSub.php?masterMeterSubSubId={$meterSubSubData[sec].masterMeterSubSubId}" class="link">Edit</a>
  </td>
  <td align="center" class="table2">{$meterSubSubData[sec].parameterName}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].range}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].accuracySubSubRdg}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].accuracySubSubFS}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].resolution}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].stability}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].uncertinty}</td>
  <td align="center" class="table2">{$meterSubSubData[sec].certiAccuracy}</td>
  <td align="center" class="table2">
    <a href="deleteMMSubSubDelete.php?masterMeterSubSubId={$meterSubSubData[sec].masterMeterSubSubId}" class="link">Delete</a>
  </td>
</tr>
{/section}
</table>