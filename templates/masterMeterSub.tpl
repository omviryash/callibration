{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function(){
  if( {$isEdit} > 0 )
  {
    getParameter();
    getDateByPara()
  }
  $(".add").click(function(){
    $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');
  });
  $(".delete").click(function(){ 
  	$(this).parent().remove();
  });
  
  $("#masterMeterId").change(function(){
    getParameter();
  });
});
function getParameter()
{
  var dataString = "masterMeterId=" + $("#masterMeterId").val() + "&masterMeterSubId=" + $(".masterMeterSubId").val();
	$.ajax({
	 type: "GET",
	 url:  "masterMeterJq.php",
	 data: dataString,
	 success: function(data){ 
	 	$("#masterpara").html(data);
	 }
	});
}

function getAllDate()
{
	 var dataString = "masterMeterId=" + $("#masterMeterId").val();
   $.ajax({
   	 type: "POST",
     url:  "masterMeterAllData.php",
     data: dataString,
     success: function(data){ 
     	$(".allData").html(data);
     }
   });
}
function getDateByPara()
{
	 var dataString = "masterMeterId=" + $('#masterMeterId').val() + "&masterMeterSubId=" + $('#masterMeterSubId').val();
   $.ajax({
   	 type: "POST",
     url:  "masterMeterAllData.php",
     data: dataString,
     success: function(data){ 
     	$(".allData").html(data);
     }
   });
}
</script>
<form name="form1" action="{$smarty.server.PHP_SELF}" id="form1" method="post">
{include file="./headEnd.tpl"}
<table border = "5" align="center" cellpadding="1" cellspacing="1">
<tr>
  <td align="center" class="table1" colspan="4">Master Meter Entry</td>
</tr>
<tr>
  <td align="right" class="table2">Meter Selection</td>
  <td>
  	<select name="masterMeterId" id="masterMeterId" onchange="getAllDate();">
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$editArray.masterMeterId}
    </select>
 	</td>
 	<td><div id="masterpara"></div></td>
	</tr>
</table>
<table border="2" width="100%" id="dataTable" align="center" left="10"cellpadding="0" cellspacing="0">
  <tbody id="mainDiv">
    <tr id="one" class="itemRow">	
      <td>
        <input type="hidden" name="masterMeterSubSubId" class="masterMeterSubSubId" value="{$editArray.masterMeterSubSubId}" />
        <input type="hidden" name="masterMeterSubIdEdit" class="masterMeterSubId" value="{$editArray.masterMeterSubId}" />
      	Range
          <input type="text" name="range[]" class="range" size="4" value="{$editArray.range}" />
        Accu. % of Rdg.
          <input type="text" name="accuracySubSub[]" class="accuracySubSub" size="5" value="{$editArray.accuracySubSub}" />
        Accu. % of FS
          <input type="text" name="accuracySubSubFS[]" class="accuracySubSubFS" size="5" value="{$editArray.accuracySubSubFS}" />
        Resolution
          <input type="text" name="resolution[]" class="resolution"  size="5" value="{$editArray.resolution}"/>
        Stability
          <input type="text" name="stability[]" class="stability" size="4" value="{$editArray.stability}" />
        Uncertinity
          <input type="text" name="uncertinty[]" class="uncertinty"  size="5" value="{$editArray.uncertinty}"/>
        <!--certiAccuracy
          <input type="text" name="certiAccuracy[]" class="certiAccuracy"  size="5" value="{$editArray.certiAccuracy}"/>
          -->
        Degree of Freedom
          <input type="text" name="degreeOfFreedom[]" class="degreeOfFreedom" size="3" value="{$editArray.degreeOfFreedom}" />
        <input type="button" name="add[]" value="Add Param" class="add" />
        <input type="button" name="delete[]" value="Delete" class="delete"/>
      </td>
    </tr>
   </tbody>
</table>
<table border="2" align="center">
  <tr>
    <td><input type="submit" name="insertBtn" value="S  T  O  R  E" class="button" /></td>
  </tr>
</table>
</form>
<br><br>
<div class="allData"></div>

<script type="text/javascript"> 
  var mutli_education = document.form1.elements["range[]"].value;
  mutli_education.setAttributeNodeNS(mutli_education.cloneNode(true));
</script>

{include file="footer.tpl"}