{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function() {
  $('#grnId').focus();
  $("#grnId").change(function(){
    var datastring = "grnId=" + $('#grnId').val();
    $.ajax({
      url: "./observationGrnAj.php",
      data: datastring,
      success: function(data){
        $('#instrument').html(data);
        $("#grnDetailId").change(function(){
          getDetails();
        });
      }
    });
  });
});
function getDetails()
{
  var datastring = "grnId=" + $('#grnId').val() + "&grnDetailId=" + $('#grnDetailId').val();
	$.ajax({
	  url: "./obsDetailAj.php",
	  data: datastring,
	  success: function(response){
	    $('#obsDetail').html(response);
	  }
	});
}

function deleteObs(grnObsId)
{
	var answer = confirm("Are You Sure Delete Your Recored ?")
	if (answer == true)
	{
	  var datastring = "grnObservationId=" + grnObsId;
		$.ajax({
		  url: "./obsDetailDelete.php",
		  data: datastring,
		  method: 'POST',
		  success: function(response){
		    getDetails();
		  }
		});
  }
	return false;
}
</script>
{include file="./headEnd.tpl"}
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<center class="center"><h2>Observation  List</h2></center><br>
<tr>
  <td class="table1" align="center">GRN NO</td>
  <td class="table1" align="center">Customer Id</td>
</tr>
<tr>
  <td>
    <select name="grnId" id="grnId">
      <option value="0">GRN List</option>
      {html_options values=$grnId output=$grnPrefixNo}
    </select>
  </td>
  <td>
    <span id="instrument">
      <select name="grnDetailId" id="grnDetailId">
        <option>Select Instrument</option>
      </select>
    </span>
  </td>
</tr>
</table>
<div id="obsDetail"></div>