{include file="./headStart.tpl"}
<script type="text/javascript">
$(document).ready(function(){
	getMasterMeterData();
	getGrnData();
	var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val() + "&masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
  $.ajax({
    type: "GET",
    url:  "masterMeterJqSubEdit.php",
    data: dataString,
    success: function(data){
      $("#masterparasub").html(data);
    }
  });
	
	var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
  $.ajax({
	  type: "GET",
	  url:  "masterMeterJqSubAccuracy.php",
	  data: dataString,
	  success: function(data1){ 
	  	$("#rangedisplay").html(data1);
	  }
	});
	
	var datastring = "grnDetailId=" + $("#grnDetailId").val();
  $.ajax({
    url: "./obsSheetAjGrnDetailChange.php",
    data: datastring,
    success: function(data){
      $('#showDetail').html(data);
    }
  });
	
  $(".add").live('click',function(){
    $("#one").clone(true).appendTo("#mainDiv").find('input[type="text"]').val('');
  });
  $(".delete").click(function(){
    $(this).parent().parent().parent().remove();
    return false;
  });
});

function getGrnData()
{
	var datastring = "grnId=" + $("#grnId").val() + "&grnDetailId=" + $("#grnDetailId").val();
	$.ajax({
	  url: "./obsSheetAjEdit.php",
	  data: datastring,
	  success: function(data){ 
	    $('#showItem').html(data);
	    $('#custCode').val($('#custCodeHidden').val());
	    //
	    $("#grnDetailId").change(function(){
	      var datastring = "grnDetailId=" + $("#grnDetailId").val();
	      $.ajax({
	        url: "./obsSheetAjGrnDetailChange.php",
	        data: datastring,
	        success: function(data){
	          $('#showDetail').html(data);
	        }
	      });
	    });
	  }
	});
}
function getMasterMeterData()
{
	var dataString = "masterMeterId=" + $("#masterMeterId").val() + "&masterMeterSubId=" + $("#masterMeterSubId").val();
	$.ajax({
	  type: "GET",
	  url:  "masterMeterJqEdit.php",
	  data: dataString,
	  success: function(data){ 
	    $("#masterpara").html(data);
	    $("#masterMeterSubId").focus();
	    
	    // Ajax Call At Range :Start 
	    $("#masterMeterSubId").change(function(){ 
	      var dataString = "masterMeterSubId=" + $("#masterMeterSubId").val();
	      $.ajax({
	        type: "GET",
	        url:  "masterMeterJqSubEdit.php",
	        data: dataString,
	        success: function(data){
	           $("#masterparasub").html(data);
	           $("#masterMeterSubSubId").focus();
	          // Get accuracysub With Ajax  :Start 
	          $("#masterMeterSubSubId").change(function(){ 
	         	  var dataString = "masterMeterSubSubId=" + $("#masterMeterSubSubId").val();
	       	    $.ajax({
	       	    type: "GET",
	       	    url:  "masterMeterJqSubAccuracy.php",
	       	    data: dataString,
	       	    success: function(data1){ 
	       	    	$("#rangedisplay").html(data1);
	              $('.accuracyTaken').each(function(){
	             	//On Changes Of Range Calculate Accuracy % taking in to account
//	                calculateAccuracy($(this));
//	                resolutionForTypeB($(this));
//	                stabilityForTypeB($(this));
//	                stabilityForTypeInPerc($(this));
//	                combinedUncertinity($(this));
//	                combinedUncertinityInPerc($(this));
//	                effectiveUncertinity($(this));
//	                effectiveUncertinityInPer($(this));
//	                effectiveDegreeOfFreed($(this));
//	                meanReadingCalculate($(this));
//	                masterMeterReading($(this));
//	                error($(this));
	                });
	       	      }
	       	    });
	          });
	        }	
	      });
	    });
	    // Get accuracysub With Ajax: End
	  }
	});
}

function getUnInPer(theObjInPer)
{
  var row = $(theObjInPer).parents('.tableRow');
  var ucInPer = $(theObjInPer).val() != '' ? $(theObjInPer).val() : 0;
  var stdMeterAverage = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  
  var uncertaintyCalibration = (parseFloat(ucInPer) * parseFloat(stdMeterAverage)) / 100;
  row.find(".uncertaintyCalibration").val(uncertaintyCalibration.toFixed(4));
}

function getUnInPerSecond(theObjInPerSecond)
{
  var row = $(theObjInPerSecond).parents('.tableRow');
  var ucInPerSecond = $(theObjInPerSecond).val() != '' ? $(theObjInPerSecond).val() : 0;
  var stdMeterAverage = $(".stdMeterAverage").val() != '' ? $(".stdMeterAverage").val() : 0;
  
  var uncertaintyCalibrationInPer = (parseFloat(ucInPerSecond) * 100 / parseFloat(stdMeterAverage));
  row.find(".uncertaintyCalibrationInPer").val(uncertaintyCalibrationInPer.toFixed(4));
}

function meanReading(theStdObject)
{
  var row = $(theStdObject).parents('.tableRow');
  var stdMeterAverage2 = parseFloat($(theStdObject).val());
	valuesAvailable = 1;
	
	stdMeter1Val = row.find(".stdMeter1").val();
	stdMeter2Val = row.find(".stdMeter2").val();
	stdMeter3Val = row.find(".stdMeter3").val();
	stdMeter4Val = row.find(".stdMeter4").val();
	stdMeter5Val = row.find(".stdMeter5").val();
	stdMeterAverageObj = row.find(".stdMeterAverage").val();
  
	if(stdMeter2Val != "")  valuesAvailable++;  else  stdMeter2Val = 0;
	if(stdMeter3Val != "")  valuesAvailable++;  else  stdMeter3Val = 0;
	if(stdMeter4Val != "")  valuesAvailable++;  else  stdMeter4Val = 0;
	if(stdMeter5Val != "")  valuesAvailable++;  else  stdMeter5Val = 0;

  stdMeterAverageObj = (parseFloat(stdMeter1Val) + parseFloat(stdMeter2Val) + parseFloat(stdMeter3Val) + parseFloat(stdMeter4Val) 
                       + parseFloat(stdMeter5Val)) / valuesAvailable;
  row.find(".stdMeterAverage").val(stdMeterAverageObj);
  row.find(".accuracyTaken").val(((stdMeterAverageObj*($("#masterMeterAccuracyArray").val())+($("#masterMeterAccuracyRangeArray").val())*($("#masterMeterAccuracyFSArray").val()))/100));
  stdMeterAverageDev = stdMeterAverageObj;
  
  stdMeter1ValDeviation = (stdMeter1Val - stdMeterAverageDev) * (stdMeter1Val - stdMeterAverageDev);
  stdMeter2ValDeviation = (stdMeter2Val - stdMeterAverageDev) * (stdMeter2Val - stdMeterAverageDev);
  stdMeter3ValDeviation = (stdMeter3Val - stdMeterAverageDev) * (stdMeter3Val - stdMeterAverageDev);
  stdMeter4ValDeviation = (stdMeter4Val - stdMeterAverageDev) * (stdMeter4Val - stdMeterAverageDev);
  stdMeter5ValDeviation = (stdMeter5Val - stdMeterAverageDev) * (stdMeter5Val - stdMeterAverageDev);
  
  row.find(".standardDeviation").val((Math.sqrt((stdMeter1ValDeviation + stdMeter2ValDeviation 
                                    + stdMeter3ValDeviation + stdMeter4ValDeviation 
                                    + stdMeter5ValDeviation) / 5)).toFixed(4));

  standardUncertinity = Math.sqrt((stdMeter1ValDeviation + stdMeter2ValDeviation 
                                    + stdMeter3ValDeviation + stdMeter4ValDeviation 
                                    + stdMeter5ValDeviation) / 5)
  row.find(".standardUncertinity").val((standardUncertinity/Math.sqrt(5)).toFixed(4));
  row.find(".standardUncertinityperc").val((row.find(".standardUncertinity").val()*100/row.find(".stdMeterAverage").val()).toFixed(4));
  row.find(".degreeOfFreedom").val(0);
  row.find(".uncertinityForTypeB").val(((row.find(".uncertaintyCalibration").val()/2)*row.find(".stdMeterAverage").val()*0.01).toFixed(4));
  row.find(".uncertinityInPercentage").val(0);
  row.find(".accuracyForTypeB").val((row.find(".accuracyTaken").val() * row.find(".stdMeterAverage").val() * 0.01 / Math.sqrt(3)).toFixed(4));
  row.find(".acuuracyForTypeBPerc").val(0);
  row.find(".resolutionTypeB").val(((row.find(".resolutionTypeA").val()/2)/Math.sqrt(3)).toFixed(4));
  
  row.find(".resolutionForTypeBPerc").val(0);
  row.find(".stabilityForTypeB").val(0);
  row.find(".stabilityForTypeBInPerc").val(0);
  row.find(".combinedUncertinityInPerc").val(0);
  row.find(".effectiveUncertinityInPer").val(0);
  row.find(".effectiveDegreeOfFreed").val(0);
  row.find(".masterMeterReading").val(0);
  row.find(".effectiveUncertinity").val(0);
  
  var  standardUncertinityComined = row.find(".standardUncertinity").val();
  var  accuracyForTypeBCombined = row.find(".accuracyForTypeB").val();
  var  uncertinityForTypeBcombined = row.find(".uncertinityForTypeB").val();
  var  resolutionTypeBCombined = row.find(".resolutionTypeB").val();
  
  row.find(".combinedUncertinity").val((Math.sqrt((standardUncertinityComined * standardUncertinityComined) + (accuracyForTypeBCombined * accuracyForTypeBCombined) +
                                           (uncertinityForTypeBcombined * uncertinityForTypeBcombined) + (resolutionTypeBCombined * resolutionTypeBCombined))).toFixed(5));
  row.find(".expandedUncertinity").val((2*row.find(".combinedUncertinity").val()).toFixed(4));
  row.find(".expandedUncertinityInPre").val((2*row.find(".combinedUncertinity").val()).toFixed(4) * 100 / stdMeterAverageObj);
  row.find(".meanReading").val(row.find(".stdMeterAverage").val());
}
function testMeterAvg(theObj)
{
	var row = $(theObj).parents('.tableRow');
  
	valuesAvailable = 1;
	
	testMeter1Val = row.find(".testMeter1").val();
	testMeter2Val = row.find(".testMeter2").val();
	testMeter3Val = row.find(".testMeter3").val();
	testMeter4Val = row.find(".testMeter4").val();
	testMeter5Val = row.find(".testMeter5").val();
	
	if(testMeter2Val != "")  valuesAvailable++;  else  testMeter2Val = 0;
	if(testMeter3Val != "")  valuesAvailable++;  else  testMeter3Val = 0;
	if(testMeter4Val != "")  valuesAvailable++;  else  testMeter4Val = 0;
	if(testMeter5Val != "")  valuesAvailable++;  else  testMeter5Val = 0;

  testAverageObj = (parseFloat(testMeter1Val) + parseFloat(testMeter2Val) + parseFloat(testMeter3Val) + parseFloat(testMeter4Val) 
                       + parseFloat(testMeter5Val)) / valuesAvailable;
  row.find("#testMeterAverage").val(testAverageObj);
  //
	
}

function sameRecord()
{
	var row = $(".stdMeter1").parents('.tableRow');
	var stdMeter1Same = row.find(".stdMeter1").val();
	var stdMeter2Same = row.find(".stdMeter2").val();
	var stdMeter3Same = row.find(".stdMeter3").val();
	var stdMeter4Same = row.find(".stdMeter4").val();
	var stdMeter5Same = row.find(".stdMeter5").val();

	var testMeter1Same = row.find(".testMeter1").val();
	var testMeter2Same = row.find(".testMeter2").val();
	var testMeter3Same = row.find(".testMeter3").val();
	var testMeter4Same = row.find(".testMeter4").val();
	var testMeter5Same = row.find(".testMeter5").val();
	var rangeIdSame = $("#masterMeterAccuracyRangeArray").val();
	if(parseFloat(stdMeter1Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(stdMeter2Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(stdMeter3Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(stdMeter4Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(stdMeter5Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(testMeter1Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(testMeter2Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(testMeter3Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(testMeter4Same) <= parseFloat(rangeIdSame) && 
	   parseFloat(testMeter5Same) <= parseFloat(rangeIdSame))
	{
		document.form1.submit();
	}
	else
	{
		alert('Not a Range');
		return false;
	}
}
</script>
{include file="./headEnd.tpl"}
<form action="{$smarty.server.PHP_SELF}" id="form1" name="form1" method='POST' onsubmit="return sameRecord()";>
<input type="hidden" name="grnDetailPassId" value={$grnDetailPassId} />
<center class="center"><h2>Observation Sheet</h2></center><br>
<table border='1' cellpadding='5' cellspacing='0' align='center'>
  <tr>
    <td>GRN NO.: <b>N-<b/></font>
    	<input type="hidden" name="grnId" value={$grnIdSelected} />
      <select name="grnId" id="grnId" onchange="getGrnData();" autofocus=autofocus required="required" DISABLED >
        <option value="">GRN</option>
        {html_options values=$grnId output=$grnPrefixNo selected=$grnIdSelected}
      </select>
    <td>
      <font size="4">Cust Id No.:-</font>
      <input type="text" name="custCode" id="custCode" value="" size="2" READONLY />
    
    <td colspan="9"  NOWRAP><font size="4">Master Meter.:-</font>
     <select name="masterMeterId" id="masterMeterId" onchange="getMasterMeterData();" DISABLED >
  	 <option value="0">Select Meter</option>
       {html_options values=$meterEntryIdArray output=$meterEntryNameArray selected=$selectedMasterMeter}
    </select>
    <input type="hidden" name="masterMeterId" value="{$selectedMasterMeter}" />
    <input type="hidden" name="masterMeterSubId" id="masterMeterSubId" value="{$selectedmParameterId}" />
    <input type="hidden" name="masterMeterSubSubId" id="masterMeterSubSubId" value="{$selectedmRangeId}" />
    <input type="hidden" id="grnDetailId" name="grnDetailId" value="{$grnDetailId}" />
    <span id="masterpara"></span>         
    <span id="masterparasub"></span>  
     {html_select_date time="$callibrationDate" prefix="callibrationDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
     {html_select_date time="$nextYearDate" prefix="nextYearDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
	 Certificate Issue 
	 {if $certificateIssue == "Y"}
	 <input type="checkbox" name="certificateIssue" value="1" CHECKED />
	 {else}
	 <input type="checkbox" name="certificateIssue" value="1" />
	 {/if}
	 {html_select_date time="$certificateIssueDate" prefix="certificateIssueDate" start_year="-3" end_year="+1" month_format="%m" field_order="DMY"  day_value_format="%02d"}
    
    </td>
  </tr>
  <tr>
    <td align="center" bgcolor="lightblue">INSTRUMENT.:-<span id="showItem"></span><div id="showDetail" required=required></div></td>
    <td align="center" bgcolor="lightblue">Certificate No :<input type="text" name="selfCertiNo" id="selfCertiNo" value="{$selfCertiNo}" required=required/></td>
    <td align="center" bgcolor="lightblue">Make/Model :<input type="text" name="makeModel" id="makeModel" size="10" value="{$grnDtlArray.makeModel}" required=required/></td>
    <td align="center" bgcolor="lightblue">Instrument I.D. No. : <input type="text" name="instrumentId" id="instrumentId" size="10" value="{$grnDtlArray.instrumentId}" required=required/></td>
    <td align="center" bgcolor="lightblue">Least Count : <input type="text" name="leastCount" id="leastCount" size="10" value="{$grnDtlArray.leastCount}" required=required/></td>
    <td align="center" bgcolor="lightblue">Accuracy : <input type="text" name="accuracy" id="accuracy" size="10" value="{$grnDtlArray.accuracy}" required=required/></td>
    <td align="center" bgcolor="lightblue">Temperature : <input type="text" name="temperature" id="temperature" size="10" value="{$grnDtlArray.temperature}" required=required/></td>
    <td align="center" bgcolor="lightblue">Relative Humidity : <input type="text" name="humidity" id="humidity" size="10" value="{$grnDtlArray.humidity}" required=required/></td>
    <td align="center" bgcolor="lightblue">Date : </td>
    <td align="center" bgcolor="lightblue">User : <input type="hidden" name="" id="" value="" size="10" required=required/></td>
  </tr>    
  <tr>
  	{if $grnDtlArray.extraFields == 1}
      <td align="center" bgcolor="lightblue">Check for Extra :<br /><input type="checkbox" name="extraFields" value="1" CHECKED /></td>
    {else}
      <td align="center" bgcolor="lightblue">Check for Extra :<br /><input type="checkbox" name="extraFields" value="1" /></td>
    {/if}
    <td align="center" bgcolor="lightblue">Field 1 : <br /><input type="text" name="extraField1" value="{$grnDtlArray.extraField1}" /></td>
    <td align="center" bgcolor="lightblue">Field 2 : <br /><input type="text" name="extraField2" value="{$grnDtlArray.extraField2}" /></td>
    <td align="center" bgcolor="lightblue">Field 3 : <br /><input type="text" name="extraField3" value="{$grnDtlArray.extraField3}" /></td>
    <td align="center" bgcolor="lightblue">Field 4 : <br /><input type="text" name="extraField4" value="{$grnDtlArray.extraField4}" /></td>
    <td align="center" bgcolor="lightblue">Field 5 : <br /><input type="text" name="extraField5" value="{$grnDtlArray.extraField5}" /></td>
    <td align="center" bgcolor="lightblue">Field 6 : <br /><input type="text" name="extraField6" value="{$grnDtlArray.extraField6}" /></td>
    <td align="center" bgcolor="lightblue" colspan="3">Remarks : <br /><input type="text" name="certiRemarks" size="50" value="{$grnDtlArray.certiRemarks}" /></td>
  </tr>    
  <tr>
  	<td align="left" colspan="8" bgcolor="lightblue">
  	  Extra Block : <input type="text" name="" value="" DISABLED />
  	  Ratio : <input type="text" size="6" name="ratio1" value="{$grnDtlArray.ratio1}" /> / <input type="text" size="6" name="ratio2" value="{$grnDtlArray.ratio2}" />
  	</td>
 </tr>    
</table>
<table align="center" border='1' cellpadding='5' cellspacing='0'>
  <tr>
    <td align="center" bgcolor="lightblue" colspan="9">Type A</td>
    <td align="center" bgcolor="lightblue" colspan="19">Type B</td>
  </tr>
  <tr>
    <td align="center">UUT Val 1</td>
    <td align="center">Val 2</td>
    <td align="center">Val 3</td>
    <td align="center">Val 4</td>
    <td align="center">Val 5</td>
    <td align="center">Mean Avg.</td>
    <td align="center">Std Devia.</td>
    <td align="center">Std Uncert.</td>
    <td align="center">Std Uncert. %</td>

    <td align="center" bgcolor="lightgreen">Uncert. %</td>
    <td align="center" bgcolor="lightpink">Uncert.</td>
    <td align="center">Resol.</td>
    <td align="center">Accu. %</td>
    <td align="center">Uncert. B</td>
    <td align="center">Accu. of Cali.</td>
    <td align="center">Resol.</td>
    <td align="center">Comb. Uncer.</td>
    <td align="center">Expan. Uncer.</td>
    <td align="center">Expan. Uncer. In %</td>
    <td align="center"><b>Master Meter Value</b></td>
    <td align="center">Val 1</td>
    <td align="center">Val 2</td>
    <td align="center">Val 3</td>
    <td align="center">Val 4</td>
    <td align="center">Val 5</td>
    <td align="center">Avg.</td>
  </tr>
  <tbody id="mainDiv">
  <tr id="one" class="tableRow">
  	<input type="hidden" name="grnObsMasterIdHidden" value="{$grnObsMasterIdHidden}" >
  	<input type="hidden" name="grnObservationId" value="{$editArray.grnObservationId}" >
    <td valign="bottom" align="center"><input type="text" name="stdMeter1[]" class="stdMeter1"  size="4" align="right" title="stdMeter1" required=required onchange="meanReading(this);" value="{$editArray.stdMeter1}" ></td>
    <td valign="bottom" align="center"><input type="text" name="stdMeter2[]" class="stdMeter2"  size="4" align="right" title="stdMeter2" required=required onchange="meanReading(this);" value="{$editArray.stdMeter2}" ></td>
    <td valign="bottom" align="center"><input type="text" name="stdMeter3[]" class="stdMeter3"  size="4" align="right" title="stdMeter3" required=required onchange="meanReading(this);" value="{$editArray.stdMeter3}" ></td>
    <td valign="bottom" align="center"><input type="text" name="stdMeter4[]" class="stdMeter4"  size="4" align="right" title="stdMeter4" required=required onchange="meanReading(this);" value="{$editArray.stdMeter4}" ></td>
    <td valign="bottom" align="center"><input type="text" name="stdMeter5[]" class="stdMeter5"  size="4" align="right" title="stdMeter5" required=required onchange="meanReading(this);" value="{$editArray.stdMeter5}" ></td>
    <td valign="bottom"><input type="text" name="stdMeterAverage[]"     class="stdMeterAverage"     size="3" align="right" title="stdMeterAverage" value="{$editArray.stdMeterAverage}" READONLY></td>
    <td valign="bottom"><input type="text" name="standardDeviation[]"   class="standardDeviation"   size="3" align="right" title="standardDeviation" value="{$editArray.standardDeviation}" READONLY></td>
    <td valign="bottom"><input type="text" name="standardUncertinity[]" class="standardUncertinity" size="3" align="right" title="standardUncertinity" value="{$editArray.standardUncertinity}" READONLY></td>
    <td valign="bottom">
      <input type="text" name="standardUncertinityperc[]" class="standardUncertinityperc" size="3" align="right" title="standardUncertinityperc" value="{$editArray.standardUncertinityperc}" READONLY>
      <input type="hidden" name="degreeOfFreedom[]"       class="degreeOfFreedom"         size="3" align="right" title="degreeOfFreedom" READONLY>
    </td>

  	<td valign="bottom" align="center"><input type="text" name="uncertaintyCalibrationInPer[]" class="uncertaintyCalibrationInPer"  size="4" align="right" title="uncertaintyCalibrationInPer" onblur="getUnInPer(this);" value="{$editArray.uncertaintyCalibrationInPer}" ></td>
  	<td valign="bottom" align="center"><input type="text" name="uncertaintyCalibration[]" class="uncertaintyCalibration"  size="5" align="right" title="uncertaintyCalibration" required=required onblur="meanReading(this); getUnInPerSecond(this);" value="{$editArray.uncertaintyCalibration}" ></td>
  	<td valign="bottom" align="center"><input type="text" name="resolutionTypeA[]" class="resolutionTypeA"  size="2" align="right" title="Resolution" required=required onblur="meanReading(this);" value="{$editArray.resolutionTypeA}" ></td>
    <td valign="bottom"><input type="text" name="accuracyTaken[]"       class="accuracyTaken"       size="3" align="right" title="accuracyTaken" value="{$editArray.accuracyTaken}" READONLY></td>
    <td valign="bottom">
      <input type="text" name="uncertinityForTypeB[]"       class="uncertinityForTypeB"     size="3" align="right" title="Uncertanity of Master" value="{$editArray.uncertinityForTypeB}" READONLY>
      <input type="hidden" name="uncertinityInPercentage[]" class="uncertinityInPercentage" size="3" align="right" title="uncertinityInPercentage" value="{$editArray.uncertinityInPercentage}" READONLY>
    </td>
    <td valign="bottom">
      <input type="text" name="accuracyForTypeB[]"       class="accuracyForTypeB"     size="3" align="right" title="accuracyForTypeB" value="{$editArray.accuracyForTypeB}" READONLY>
      <input type="hidden" name="acuuracyForTypeBPerc[]" class="acuuracyForTypeBPerc" size="3" align="right" title="acuuracyForTypeBPerc" value="{$editArray.acuuracyForTypeBPerc}" READONLY>
    </td>
    <td valign="bottom">
      <input type="text"   name="resolutionTypeB[]"         class="resolutionTypeB"         size="3" align="right" title="resolutionTypeB" value="{$editArray.resolutionTypeB}" READONLY>
      <input type="hidden" name="resolutionForTypeBPerc[]"  class="resolutionForTypeBPerc"  size="3" align="right" title="resolutionForTypeBPerc" value="{$editArray.resolutionForTypeBPerc}" READONLY>   
      <input type="hidden" name="stabilityForTypeB[]"       class="stabilityForTypeB"       size="3" align="right" title="stabilityForTypeB" value="{$editArray.stabilityForTypeB}" READONLY>        
      <input type="hidden" name="stabilityForTypeBInPerc[]" class="stabilityForTypeBInPerc" size="3" align="right" title="stabilityForTypeBInPerc" value="{$editArray.stabilityForTypeBInPerc}" READONLY>  
    </td>
    <td valign="bottom">
      <input type="text"   name="combinedUncertinity[]"       class="combinedUncertinity"       size="3" align="right" title="combinedUncertinity" value="{$editArray.combinedUncertinity}" READONLY>      
      <input type="hidden" name="combinedUncertinityInPerc[]" class="combinedUncertinityInPerc" size="3" align="right" title="combinedUncertinityInPerc" value="{$editArray.combinedUncertinityInPerc}" READONLY>
      <input type="hidden" name="effectiveUncertinity[]"      class="effectiveUncertinity"      size="3" align="right" title="effectiveUncertinity" value="{$editArray.effectiveUncertinity}" READONLY>     
      <input type="hidden" name="effectiveUncertinityInPer[]" class="effectiveUncertinityInPer" size="3" align="right" title="effectiveUncertinityInPer" value="{$editArray.effectiveUncertinityInPer}" READONLY>
      <input type="hidden" name="effectiveDegreeOfFreed[]"    class="effectiveDegreeOfFreed"    size="3" align="right" title="effectiveDegreeOfFreed" value="{$editArray.effectiveDegreeOfFreed}" READONLY>   
      <input type="hidden" name="meanReading[]"               class="meanReading"               size="3" align="right" title="MeanReading" value="{$editArray.meanReading}" READONLY>              
      <input type="hidden" name="masterMeterReading[]"        class="masterMeterReading"        size="3" align="right" title="MeanReading" value="{$editArray.masterMeterReading}" READONLY>              
      <input type="hidden" name="error[]"                     class="error"                     size="3" align="right" title="Error" value="{$editArray.error}" READONLY>
    </td>
    <td valign="bottom"><input type="text" name="expandedUncertinity[]"       class="expandedUncertinity"       size="5" align="right" title="ExpandedUncertinity" value="{$editArray.expandedUncertinity}" READONLY></td>
    <td valign="bottom"><input type="text" name="expandedUncertinityInPre[]"  class="expandedUncertinityInPre"  size="5" align="right" title="ExpandedUncertinityInPer" value="{$editArray.expandedUncertinityInPre}" READONLY></td>
    <td><input type="text" size="4" style="background-color:#000000;" READONLY /></td>
    <td valign="bottom"><input type="text" name="testMeter1[]" class="testMeter1" id="testMeter1Final1" size="3" align="right" title="testMeter1" required=required onchange="testMeterAvg(this);" value="{$editArray.testMeter1}" ></td>
    <td valign="bottom"><input type="text" name="testMeter2[]" class="testMeter2" id="testMeter1Final2" size="3" align="right" title="testMeter2" required=required onchange="testMeterAvg(this);" value="{$editArray.testMeter2}" ></td>
    <td valign="bottom"><input type="text" name="testMeter3[]" class="testMeter3" id="testMeter1Final3" size="3" align="right" title="testMeter3" required=required onchange="testMeterAvg(this);" value="{$editArray.testMeter3}" ></td>
    <td valign="bottom"><input type="text" name="testMeter4[]" class="testMeter4" id="testMeter1Final4" size="3" align="right" title="testMeter4" required=required onchange="testMeterAvg(this);" value="{$editArray.testMeter4}" ></td>
    <td valign="bottom"><input type="text" name="testMeter5[]" class="testMeter5" id="testMeter1Final5" size="3" align="right" title="testMeter5" required=required onchange="testMeterAvg(this);" value="{$editArray.testMeter5}" ></td>
    <td valign="bottom"><input type="text" name="testMeterAverage[]"  id="testMeterAverage"  size="5" align="right" title="testMeter5" value="{$editArray.testMeterAverage}" READONLY></td>
  </tr>
  </tbody>
  <tr>
    <td align="center" colspan="28"><input type="submit" name="insertBtn" value="Submit" class="button" /></td>
  </tr>
</table>
<div id="rangedisplay"></div> 
</table>
</form>
{include file="footer.tpl"}